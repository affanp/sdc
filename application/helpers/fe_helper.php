<?php defined('BASEPATH') OR exit('No direct script access allowed');

function asset_frontend_url($link='')
{
	$http = $_SERVER['SERVER_PORT'] == 443 ? "https" : "http";
	if($_SERVER['HTTP_HOST'] == 'web.local' || $_SERVER['HTTP_HOST'] == 'localhost'){
		return $http."://".$_SERVER['HTTP_HOST']."/sdc/assets/frontend/".$link;
	}else{
		return $http."://".$_SERVER['HTTP_HOST']."/assets/frontend/".$link;
	}	
} 

function asset_backend_url($link='')
{
	$http = $_SERVER['SERVER_PORT'] == 443 ? "https" : "http";
	if($_SERVER['HTTP_HOST'] == 'web.local' || $_SERVER['HTTP_HOST'] == 'localhost'){
		return $http."://".$_SERVER['HTTP_HOST']."/sdc/assets/backend/".$link;
	}else{
		return $http."://".$_SERVER['HTTP_HOST']."/assets/backend/".$link;
	}	
} 

function site_url($link='')
{
	$http = $_SERVER['SERVER_PORT'] == 443 ? "https" : "http";
	if($_SERVER['HTTP_HOST'] == 'web.local' || $_SERVER['HTTP_HOST'] == 'localhost'){
		return $http."://".$_SERVER['HTTP_HOST']."/sdc/".$link;
	}else{
		return $http."://".$_SERVER['HTTP_HOST']."/".$link;
	}
}

function rand_num()
{
	if($_SERVER['HTTP_HOST'] == 'web.local' || $_SERVER['HTTP_HOST'] == 'localhost'){
		return '?n='.rand();
	}else{
		return '';
	}
}

function print_die($data)
{
	print_r('<pre>'); print_r($data); die;
}

function debug_query()
{
	$CI =& get_instance();
	echo $CI->db->last_query(); die;
}

function get_meta_robots()
{
	$meta = 'noindex, nofollow';

	if($_SERVER['HTTP_HOST'] == 'sdc1000.com' || $_SERVER['HTTP_HOST'] == 'www.sdc1000.com'){
		$meta = 'index, follow';
	}

	return $meta;
}