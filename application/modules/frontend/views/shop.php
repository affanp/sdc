<!-- <div class="nile-page-title breadcrumb-1">
    <div class="container">
        <h1>Where to buy</h1>
        <ul class="breadcrumbs">
            <li><a href="<?php echo site_url(); ?>">Home</a></li>
            <li class="active">Where to buy</li>
        </ul>

    </div>
</div> -->
<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/banner.png)" class="padding-top-120px padding-bottom-90px" id="shop" >
    
</section>
<section class="padding-top-120px padding-bottom-90px" id="shop">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-12 text-center">  
                        <div class="nile-title layout-1 text-center">
                            <div class="position-relative">
                                <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Snow" style="width:100%;">
                                <?php if($this->agent->is_mobile()):?>
                                <div class="centered title-w-bg-white title-medium uppercase">Dapetin SD C-1000 Di Sini</div>
                                <?php else:?>
                                <div class="centered title-w-bg-white title-xlarge uppercase">Dapetin SD C-1000 Di Sini</div>
                                <?php endif;?>    
                            </div> 
                            <div class="sub-title text-buy">
                                SD C-1000, bisa lo dapetin di Toko Swalayan, Apotek, atau Warung terdekat.
                                <br><br>
                                Mager ke luar rumah?
                                <br>
                                Tinggal klik di E-comerce kesayangan juga bisa!
                                <br>
                                Yuk beli sekarang, jangan tunggu daya tahan tubuh lo ngedrop!
                                <br><br>
                                Beli online di sini!
                                <a href="#" class="d-block bg-white mt-4 main-button-buy">
                                    <div class="btn-buy-white bg-white d-flex justify-content-start">
                                        <div class="btn-buy-orange">
                                            <img src="<?php echo asset_frontend_url('img/cart-white.png'); ?>" class="m40 flex-center">
                                        </div>
                                        <div>
                                            <img src="<?php echo asset_frontend_url('img/logo-shopee.png'); ?>" class="m40 flex-center">
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="d-block bg-white mt-3 main-button-buy">
                                    <div class="btn-buy-white bg-white d-flex justify-content-start">
                                        <div class="btn-buy-orange">
                                            <img src="<?php echo asset_frontend_url('img/cart-white.png'); ?>" class="m40 flex-center">
                                        </div>
                                        <div>
                                            <img src="<?php echo asset_frontend_url('img/logo-jdid.png'); ?>" class="m40 flex-center">
                                        </div>
                                    </div>
                                </a>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-5">
                <div class="fizo-image layout-2">
                    <img src="<?php echo asset_frontend_url('img/image-3.png'); ?>" alt="">
                </div>
            </div>
        </div>   
    </div>
</section>