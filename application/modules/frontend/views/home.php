<style>
    #readMore:hover{
        color: #F37B2F;
    }
</style>
<section>
    <div id="home-slider" class="owl-carousel">
        <?php if(!empty($res_slider)): foreach($res_slider as $rs): ?>
        <div class="position-relative"> 
            <img src="<?php echo !empty($rs['image']) ? asset_backend_url('uploads/logo/'.$rs['image']) : asset_frontend_url('img/slider_images/banner-website.png'); ?>"> 
            <div style="position: absolute;left: 0;top: 0;padding: 5%;width: 50%;height: 100%;">
                <div class="position-relative text-center">
                    <img src="<?php echo asset_frontend_url('img/mobile/Brush Hijau.png');?>" alt="<?php echo $rs['title']; ?>" class="slider-title-bg">
                    <h2 class="centered title-w-bg-white slider-title"><?php echo $rs['title']; ?></h2>
                </div>
                <p class="slider-description mt-5 mb-5"><?php echo $rs['description']; ?></p>
                <a href="<?php echo !empty($rs['link']) ? $rs['link'] : '#'; ?>" class="btn-orange">Coba Gratis</a>
            </div>
        </div>
        <?php endforeach; else: ?>
        <div class="position-relative"> 
            <img src="<?php echo asset_frontend_url('img/slider_images/banner-website.png'); ?>"> 
            <div style="position: absolute;left: 0;top: 0;padding: 5%;width: 50%;height: 100%;">
                <div class="position-relative text-center">
                    <img src="<?php echo asset_frontend_url('img/mobile/Brush Hijau.png');?>" alt="Bodyguard Tubuh Biar Ga Ngedrop" class="slider-title-bg">
                    <h2 class="centered title-w-bg-white slider-title">Bodyguard Tubuh Biar Ga Ngedrop</h2>
                </div>
                <p class="slider-description mt-5 mb-5">SD C-1000, vitamin penjaga daya tahan tubuh dengan triple proteksi yang mengandung vitamn C 1000mg, madu dan ekstrak mint yang segar buat ningktin imunitas tubuh lo.<br>Apapun aktivitas new norml Lo, SD C-1000 bodyguardnya.</p>
                <a href="#" class="btn-orange">Coba Gratis</a>
            </div>
        </div>
        <?php endif; ?> 
    </div>
</section>

<section class="padding-top-50px padding-bottom-100px position-relative" style="background-image: url('<?php echo asset_frontend_url('img/bgbiru.jpg');?>');" id="product-duo">
    <!-- <img src="<?php echo asset_frontend_url('img/bgbiru.jpg');?>" style="position: absolute;width: 100%;height: 100%;"> -->
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-7">
                <div class="nile-title layout-1 text-center">
                    <div class="bg-brush-bold">
                        <h2 class="text-white mb-0 title-xlarge">2 Rasa Segar Buat Jaga</br>Daya Tahan Tubuh Lo!</h2>
                    </div>    
                </div>
            </div>
            <div class="col"></div>
        </div>
        
       <!--  <div style="position: absolute;width: 100%;left: 0;"> -->
            <div class="row content mt-0">
                <div class="col pt-5r">
                    <h3 class="text-right text-dark-orange">SD C-1000 ORANGE</h3>
                    <p class="desc text-right">Varian rasa baru yang nyegerin banget dari SD C-1000. Dengan rasa jeruk yang enggak cuma bisa nyegerin mood lo, tapi juga baik untuk bantu jaga daya tahan tubuh lo.</p>
                </div>
                <div class="col-6 text-center">
                    <img src="<?php echo asset_frontend_url(); ?>img/productbaru.png" class="w-100">
                </div>
                <div class="col pt-5r">
                    <h3 class="text-left text-green">SD C-1000 JERUK NIPIS</h3>
                    <p class="desc text-left">SD C-1000 Jeruk Nipis mengandung vitamin C 1000mg, madu, ekstrak mint dan jeruk nipis asli yang siap bantu jagain imunitas lo biar ga gampang ngedrop.</p>
                </div>
            </div>


            <div class="text-center mt-10">
                <a href="<?php echo site_url("shop"); ?>" class="btn btn-circle btn-orange">Beli Sekarang</a>
            </div>
        <!-- </div> -->

        <!-- <div style="position: absolute;width: 100%;left: 0;"> -->
            <div class="row margin-top-180px triple-protection">
                <div class="col"></div>
                <div class="col-7 text-center">
                    <div class="nile-title layout-1 text-center">
                        <div class="bg-brush-orange bg-brush-p" style="padding: 19px 1% !important;">
                            <h2 class="text-white mb-0 title-xlarge">SIAP AKSI DENGAN</h2>
                        </div>    
                    </div>
                    <img class="m40" src="<?php echo asset_frontend_url(); ?>img/triple-proteksi-sdc1.png">
                </div>
                <div class="col"></div>
            </div>

            <div class="row">
                <div class="col"></div>
                <div class="col-lg-8 text-center" style="min-width: 450px;">
                    <img class="ingridient" src="<?php echo asset_frontend_url('img/ingridient-2.png'); ?>" style="width: inherit;">
                </div>  
                <div class="col"></div>
            </div>

            <div class="d-flex justify-content-between mb-5">
                <div class="text-right w-33" style="margin-top: -158px;">
                    <img class="m40" src="<?php echo asset_frontend_url('img/left-arrow.png'); ?>" style="margin-right: 34%;">
                    <div class="position-relative text-center mh-21">
                        <img src="<?php echo asset_frontend_url('img/shape-ingridient.png'); ?>" class="img-fluid mh-21">
                        <div class="text-white centered pl-5 pr-5">
                            <h3 class="title-ingridient">Vitamin C 1000 mg</h3>
                            <p>Vitamin C 1000mg sebagai amunisi tubuh Lo biar fit setiap hari.</p>
                        </div>
                    </div>
                </div>
                <div class="text-center w-33">
                    <img class="m40" src="<?php echo asset_frontend_url('img/mid-arrow.png'); ?>">
                    <div class="position-relative text-center mh-21">
                        <img src="<?php echo asset_frontend_url('img/shape-ingridient.png'); ?>" class="img-fluid mh-21">
                        <div class="text-white centered pl-5 pr-5">
                            <h3 class="title-ingridient">Madu</h3>
                            <p>Madu terkenal ampuh untuk jaga stamina dan imunitas tubuh.</p>
                        </div>    
                    </div>
                </div>
                <div class="text-left w-33" style="margin-top: -167px;">
                    <img class="m40" src="<?php echo asset_frontend_url('img/right-arrow.png'); ?>" style="margin-left: 34%;">
                    <div class="position-relative text-center mh-21">
                        <img src="<?php echo asset_frontend_url('img/shape-ingridient.png'); ?>" class="img-fluid mh-21">
                        <div class="text-white centered pl-5 pr-5">
                            <h3 class="title-ingridient">Ekstrak Mint</h3>
                            <p>Ekstrak mint yang kaya antioksidan bantu tubuh lo biar enggak ngedrop.</p>
                        </div>    
                    </div>
                </div>
            </div>
        <!-- </div> -->    
    </div>
</section>   

<section>
    <div id="video-home">
    <iframe frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"width="788.54" height="443" type="text/html" src="https://www.youtube.com/embed/FUPkD-bnqug?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=1&cc_load_policy=0&start=0&end=0" style="width: 100%;min-height: 600px;"></iframe>
        <!-- <a href="https://www.youtube.com/watch?v=ZQVgSftJt9E" class="button-play yt-preview">
            <svg version="1.1" id="play" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="200px" width="200px"
                 viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
              <path class="stroke-solid" fill="none" stroke="#ddbe72"  d="M49.9,2.5C23.6,2.8,2.1,24.4,2.5,50.4C2.9,76.5,24.7,98,50.3,97.5c26.4-0.6,47.4-21.8,47.2-47.7
                C97.3,23.7,75.7,2.3,49.9,2.5"/>
              <path class="icon" fill="#ddbe72" d="M38,69c-1,0.5-1.8,0-1.8-1.1V32.1c0-1.1,0.8-1.6,1.8-1.1l34,18c1,0.5,1,1.4,0,1.9L38,69z"/>
            </svg>
        </a> -->
        <!-- <div class="video-btn">
            <a href="https://www.youtube.com/watch?v=ZQVgSftJt9E" class="video-play-button  yt-preview"><span></span></a>
        </div> -->
    </div>
    <!-- <iframe frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"width="788.54" height="443" type="text/html" src="https://www.youtube.com/embed/FUPkD-bnqug?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=1&cc_load_policy=0&start=0&end=0" style="width: 100%;min-height: 600px;"></iframe> -->
</section>   
<!-- url(../../frontend/img/bgarticlebaru.jpg) -->
<section id="blog" class="padding-tb-100px background-white with-bg" style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/bgarticlebaru.jpg)">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-7 text-center">
                <div class="nile-title layout-1 text-center">
                    <div class="bg-brush-green bg-brush-p">
                        <h2 class="text-white mb-0 title-xlarge">ARTIKEL</h2>
                    </div> 
                    <div class="sub-title">Baca info-info menarik dan up to date di sini!</div>   
                </div>
            </div>  
            <div class="col"></div>
        </div>
        
        <div class="row content">
            <?php if(!empty($res_articles)): foreach($res_articles as $ra): ?>
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <?php if(!empty($ra['image'])):?>
                        <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/article/'.$ra['image']) ? asset_backend_url('uploads/article/'.$ra['image']) : asset_frontend_url('img/tes/600x450.png'); ?>" alt="<?php echo $ra['title']; ?>">
                        <?php else: ?>
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <?php endif; ?>    
                        <!-- <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="read-more flex-center">Read More</a> -->
                    </div>
                    <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="title_in ml-3 mr-3 pb-3"><?php echo $ra['title']; ?></a>
                    <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endforeach; else: ?>    
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-10">
            <a href="<?php echo site_url("artikel"); ?>" class="btn btn-circle btn-orange">Artikel Lainnya</a>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col"></div>
            <div class="col-7 text-center">
                <div class="nile-title layout-1 text-center">
                    <div class="bg-brush-green bg-brush-p">
                        <h2 class="text-white mb-0 title-xlarge">NEWS & EVENTS</h2>
                    </div> 
                    <div class="sub-title">Cek event SD C-1000 di kotamu!</div>   
                </div>
            </div>
            <div class="col"></div>
        </div>

        <div class="row content">
            <?php if(!empty($res_news_events)): foreach($res_news_events as $rne): ?>
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <?php if(!empty($rne['image'])):?>
                        <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/article/'.$rne['image']) ? asset_backend_url('uploads/article/'.$rne['image']) : asset_frontend_url('img/tes/600x450.png'); ?>" alt="<?php echo $rne['title']; ?>">
                        <?php else: ?>
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <?php endif; ?>    
                        <!-- <a href="<?php echo site_url('berita/detail/'.$rne['slug']); ?>" class="read-more flex-center">Read More</a> -->
                    </div>
                    <a href="<?php echo site_url('berita/detail/'.$rne['slug']); ?>" class="title_in ml-3 mr-3 pb-3"><?php echo $rne['title']; ?></a>
                    <a href="<?php echo site_url('berita/detail/'.$rne['slug']); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endforeach; else: ?>    
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-10">
            <a href="<?php echo site_url("berita"); ?>" class="btn btn-circle btn-orange">Berita Lainnya</a>
        </div>
    </div>
</section>         