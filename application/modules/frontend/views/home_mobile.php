<section>
    <div id="home-slider" class="owl-carousel">
        <?php if(!empty($res_slider)): foreach($res_slider as $rs): ?>
        <div class="position-relative"> 
            <img src="<?php echo !empty($rs['image_mobile']) ? asset_backend_url('uploads/logo/'.$rs['image_mobile']) : asset_frontend_url('img/slider_images/banner-website.png'); ?>"> 
            <div style="position: absolute;left: 0;top: 0;padding: 5%;width: 50%;height: 100%;">
                <div class="position-relative text-center">
                    <img src="<?php echo asset_frontend_url('img/mobile/Brush Hijau.png');?>" alt="<?php echo $rs['title']; ?>" class="slider-title-bg">
                    <h2 class="centered title-w-bg-white slider-title"><?php echo $rs['title']; ?></h2>
                </div>
                <p class="slider-description mt-1 mb-2"><?php echo $rs['description']; ?></p>
                <a href="<?php echo !empty($rs['link']) ? $rs['link'] : '#'; ?>" class="btn-orange-sm">Coba Gratis</a>
            </div>
        </div>
        <?php endforeach; else: ?>
        <div class="position-relative"> 
            <img src="<?php echo asset_frontend_url('img/slider_images/banner-website.png'); ?>"> 
            <div style="position: absolute;left: 0;top: 0;padding: 5%;width: 50%;height: 100%;">
                <div class="position-relative text-center">
                    <img src="<?php echo asset_frontend_url('img/mobile/Brush Hijau.png');?>" alt="Bodyguard Tubuh Biar Ga Ngedrop" class="slider-title-bg">
                    <h2 class="centered title-w-bg-white slider-title">Bodyguard Tubuh Biar Ga Ngedrop</h2>
                </div>
                <p class="slider-description mt-1 mb-2">SD C-1000, vitamin penjaga daya tahan tubuh dengan triple proteksi yang mengandung vitamn C 1000mg, madu dan ekstrak mint yang segar buat ningktin imunitas tubuh lo.<br>Apapun aktivitas new norml Lo, SD C-1000 bodyguardnya.</p>
                <a href="#" class="btn-orange-sm">Coba Gratis</a>
            </div>
        </div>
        <?php endif; ?> 
    </div>
</section>

<section class="padding-top-50px padding-bottom-50px mobile" id="product-duo">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-12 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/brush-headline-biru-tua.png" alt="" class="mh85px">
                        <div class="centered title-w-bg">2 Rasa Segar Buat Jaga</br>Daya Tahan Tubuh Lo!</div>
                    </div> 
                </div>
            </div>   
            <div class="col"></div>
        </div>
        
        <div class="row mt-0">
            <div class="col align-center">
                <!-- <img src="<?php echo asset_frontend_url(); ?>img/mobile/SDC-Orange.png"> -->
                <img src="<?php echo asset_frontend_url(); ?>img/SDC500-2.png">
            </div>
            <div class="col-7">
                <h5 class="text-left text-dark-orange">SD C-1000 ORANGE</h5>
                <p class="text-left">Varian rasa baru yang nyegerin banget dari SD C-1000. Dengan rasa jeruk yang enggak cuma bisa nyegerin mood lo, tapi juga baik untuk bantu jaga daya tahan tubuh lo.</p>
            </div>
            
        </div>

        <div class="row mt-5">
            <div class="col-7">
                <h5 class="text-right text-green">SD C-1000 JERUK NIPIS</h5>
                <p class="text-right">SD C-1000 Jeruk Nipis mengandung vitamin C 1000mg, madu, ekstrak mint dan jeruk nipis asli yang siap bantu jagain imunitas lo biar ga gampang ngedrop.</p>
            </div>
            <div class="col align-center">
                <!-- <img src="<?php echo asset_frontend_url(); ?>img/mobile/SDC-Jeruk-Nipis.png"> -->
                <img src="<?php echo asset_frontend_url(); ?>img/SDC500-1.png">
            </div>
        </div>

        <div class="text-center mt-10 pb-7">
            <a href="<?php echo site_url("shop"); ?>" class="btn btn-circle btn-orange shadow-orange">Beli Sekarang</a>
        </div>

        <div class="row margin-top-180px triple-protection">
            <div class="col"></div>
            <div class="col-9 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Orange.png" alt="Snow" style="width:100%;">
                        <div class="centered title-w-bg-white">SIAP AKSI DENGAN</div>
                    </div> 
                </div>
                <img class="m60" src="<?php echo asset_frontend_url(); ?>img/triple-proteksi-sdc1.png">
            </div>   
            <div class="col"></div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-12">
                <div class="card bg-green border-radius-30 shad-in-out">
                    <div class="row no-gutters article-card">
                        <div class="col-4 align-center">
                            <img src="<?php echo asset_frontend_url(); ?>img/mobile/Jeruk.png" class="img-fluid ingridient" alt="VITAMIN C 1000 MG">
                        </div>
                        <div class="col">
                            <div class="card-block p-3 text-white">
                                <h4>VITAMIN C 1000 MG</h4>
                                <p class="card-text">Vitamin C 1000mg sebagai amunisi tubuh Lo biar fit setiap hari.</p>      
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card bg-green border-radius-30 shad-in-out">
                    <div class="row no-gutters article-card">
                        <div class="col-4 align-center">
                            <img src="<?php echo asset_frontend_url(); ?>img/mobile/Madu.png" class="img-fluid ingridient" alt="Madu">
                        </div>
                        <div class="col">
                            <div class="card-block p-3 text-white">
                                <h4>MADU</h4>
                                <p class="card-text">Madu terkenal ampuh untuk jaga stamina dan imunitas tubuh.</p>
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card bg-green border-radius-30 shad-in-out">
                    <div class="row no-gutters article-card">
                        <div class="col-4 align-center">
                            <img src="<?php echo asset_frontend_url(); ?>img/mobile/Mint.png" class="img-fluid ingridient" alt="EKSTRAK MINT">
                        </div>
                        <div class="col">
                            <div class="card-block p-3 text-white">
                                <h4>EKSTRAK MINT</h4>
                                <p class="card-text">Ekstrak mint yang kaya antioksidan bantu tubuh lo biar enggak ngedrop.</p>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>
</section>    

<section>
    <!-- <iframe frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"width="788.54" height="200" type="text/html" src="https://www.youtube.com/embed/FUPkD-bnqug?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=1&cc_load_policy=0&start=0&end=0" style="width: 100%;"></iframe> -->
    <div id="video-home">
        <img src="https://i.ytimg.com/vi/ZQVgSftJt9E/hq720.jpg" alt="" class="w-100">
        <div class="video-btn">
            <a href="https://www.youtube.com/watch?v=ZQVgSftJt9E" class="video-play-button  yt-preview"><span></span></a>
        </div>
    </div>    
</section>   

<section id="blog" class="padding-tb-100px background-white with-bg-m">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-9 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Snow" style="width:100%;">
                        <div class="centered title-w-bg-white">ARTIKEL</div>
                    </div> 
                    <div class="sub-title">Baca info-info menarik dan up to date di sini!</div> 
                </div>
            </div>   
            <div class="col"></div>
        </div>
        
        <div class="row">
            <?php if(!empty($res_articles)): foreach($res_articles as $kra => $ra): ?>
            <?php if($kra == 0): ?>    
            <div class="col-sm-12">
            <?php else: ?> 
            <div class="col-sm-12 mt-2">
            <?php endif; ?>    
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/article/'.$ra['image']) ? asset_backend_url('uploads/article/'.$ra['image']) : asset_frontend_url('img/tes/200.png'); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text"><?php echo $ra['title']; ?></p>
                                <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php endforeach; else: ?>     
            <div class="col-sm-12">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("artikel/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-20">
            <a href="<?php echo site_url("artikel"); ?>" class="btn btn-circle btn-orange shadow-orange more-article" style="padding: 7px 40px;">Artikel Lainnya</a>
        </div>
    </div>

    <div class="container mt-5 padding-tb-50px">
        <div class="row">
            <div class="col"></div>
            <div class="col-9 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Snow" style="width:100%;">
                        <div class="centered title-w-bg-white">NEWS & EVENTS</div>
                    </div> 
                    <div class="sub-title">Cek event SD C-1000 di kotamu!</div> 
                </div>
            </div>   
            <div class="col"></div>
        </div>

        <div class="row">
            <?php if(!empty($res_news_events)): foreach($res_news_events as $krne => $rne): ?>
            <?php if($krne == 0): ?>    
            <div class="col-sm-12">
            <?php else: ?> 
            <div class="col-sm-12 mt-2">
            <?php endif; ?> 
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/article/'.$rne['image']) ? asset_backend_url('uploads/article/'.$rne['image']) : asset_frontend_url('img/tes/200.png'); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text"><?php echo $rne['title']; ?></p>
                                <a href="<?php echo site_url('berita/detail/'.$rne['slug']); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php endforeach; else: ?>
            <div class="col-sm-12">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card border-radius-25">
                    <div class="row no-gutters article-card shadow-orange border-radius-25">
                        <div class="col-4">
                            <img src="<?php echo asset_frontend_url('img/tes/200.png');?>" class="img-fluid" alt="">
                        </div>
                        <div class="col">
                            <div class="card-block p-3">
                                <p class="card-text">Description</p>
                                <a href="<?php echo site_url("berita/detail/tes-artikel"); ?>" class="read-more-mobile text-right">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-20">
            <a href="<?php echo site_url("berita"); ?>" class="btn btn-circle btn-orange shadow-orange more-news" style="padding: 7px 40px;">Berita Lainnya</a>
        </div>
    </div>
</section>         