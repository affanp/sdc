<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/banner.png)" class="padding-top-120px padding-bottom-90px" id="shop" >
    
</section>
<section id="blog" class="padding-tb-100px background-white with-bg">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <?php if($this->agent->is_mobile()):?>
            <div class="col-9 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="video" style="width:100%;">
                        <div class="centered title-w-bg-white">VIDEOS</div>
                    </div> 
                    <div class="sub-title">Lorem ipsum dolor sit amet</div> 
                </div>
            </div>   
            <?php else:?>
            <div class="col-4 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="video" style="width:100%;">
                        <div class="centered title-w-bg-white title-xlarge">VIDEOS</div>
                    </div> 
                    <div class="sub-title">Lorem ipsum dolor sit amet</div> 
                </div>
            </div>    
            <?php endif;?>
            <div class="col"></div>
        </div>

        <div class="row content">
            <?php if(!empty($res_videos)): foreach($res_videos as $rv): ?>
            <div class="col-lg-4">
                <div class="fizo-blog layout-2">
                    <div class="img-in">
                        <img src="https://i.ytimg.com/vi/ZQVgSftJt9E/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAOIoNpBn40865aTLHxFx2rpQhh6w" alt="" class="bottom-rad w-100">
                        <div class="video-btn">
                            <a href="<?php echo $rv['link']; ?>" class="video-play-button  yt-preview"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; else: ?>     
            <div class="col-lg-4">
                <div class="fizo-blog layout-2">
                    <div class="img-in">
                        <img src="https://i.ytimg.com/vi/ZQVgSftJt9E/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAOIoNpBn40865aTLHxFx2rpQhh6w" alt="" class="bottom-rad w-100">
                        <div class="video-btn">
                            <a href="https://www.youtube.com/watch?v=ZQVgSftJt9E" class="video-play-button  yt-preview"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2">
                    <div class="img-in">
                        <img src="https://i.ytimg.com/vi/UAg-ET1vES8/hqdefault.jpg?sqp=-oaymwEcCOADEI4CSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDsut4Tk5jEXwDnfVv9RjkijExFKQ" alt="" class="bottom-rad w-100">
                        <div class="video-btn">
                            <a href="https://www.youtube.com/watch?v=FUPkD-bnqug" class="video-play-button  yt-preview"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2">
                    <div class="img-in">
                        <img src="https://i.ytimg.com/vi/7GoC90hBZ4I/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLANL-6Qi6q2Er2pYYwhcVGeUhGBeA" alt="" class="bottom-rad w-100">
                        <div class="video-btn">
                            <a href="https://www.youtube.com/watch?v=7GoC90hBZ4I" class="video-play-button  yt-preview"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-5">
            <ul class="pagination">
                <?php if(!empty($res_videos)): ?>
                    <?php echo $pagination; ?>
                <?php else: ?>    
                <li class="page-item"><a class="page-link" href="#"><</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">...</a></li>
                <li class="page-item"><a class="page-link" href="#">10</a></li>
                <li class="page-item"><a class="page-link" href="#">></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section> 