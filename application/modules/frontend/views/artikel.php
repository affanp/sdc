<style>
    #readMore:hover{
        color: #F37B2F;
    }
</style>
<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/banner.png)" class="padding-top-120px padding-bottom-90px" id="shop" >
    
</section>
<section id="blog" class="padding-tb-100px background-white with-bg">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <?php if($this->agent->is_mobile()):?>
            <div class="col-9 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Snow" style="width:100%;">
                        <div class="centered title-w-bg-white">ARTIKEL</div>
                    </div> 
                    <div class="sub-title">Baca info-info menarik dan up to date di sini!</div> 
                </div>
            </div>   
            <?php else:?>
            <div class="col-4 text-center">  
                <div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Snow" style="width:100%;">
                        <div class="centered title-w-bg-white title-xlarge">ARTIKEL</div>
                    </div> 
                    <div class="sub-title">Baca info-info menarik dan up to date di sini!</div> 
                </div>
            </div>    
            <?php endif;?>  
            <div class="col"></div>
        </div>

        <div class="row content">
            <?php if(!empty($res_articles)): foreach($res_articles as $ra): ?>
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <?php if(!empty($ra['image'])):?>
                        <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/article/'.$ra['image']) ? asset_backend_url('uploads/article/'.$ra['image']) : asset_frontend_url('img/tes/600x450.png'); ?>" alt="<?php echo $ra['title']; ?>">
                        <?php else: ?>
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <?php endif; ?>    
                        <!-- <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="read-more flex-center">Read More</a> -->
                    </div>
                    <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="title_in ml-3 mr-3 pb-3"><?php echo $ra['title']; ?></a>
                    <a href="<?php echo site_url('artikel/detail/'.$ra['slug']); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endforeach; else: ?> 
            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="#" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="#" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <!-- <div class="excerpt">If you’re like most consultants you are a one-man-band operating on a shoe string budget. You don’t have thousa ...</div> -->
                    <a href="#" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="#" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="#" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <!-- <div class="excerpt">If you’re like most consultants you are a one-man-band operating on a shoe string budget. You don’t have thousa ...</div> -->
                    <a href="#" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-blog layout-2 shadow-orange">
                    <div class="img-in">
                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
                        <a href="#" class="read-more flex-center">Read More</a>
                    </div>
                    <a href="#" class="title_in ml-3 mr-3 pb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                    <!-- <div class="excerpt">If you’re like most consultants you are a one-man-band operating on a shoe string budget. You don’t have thousa ...</div> -->
                    <a href="#" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="text-center mt-5">
            <ul class="pagination">
                <?php if(!empty($res_articles)): ?>
                    <?php echo $pagination; ?>
                <?php else: ?> 
                <li class="page-item"><a class="page-link" href="#"><</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">...</a></li>
                <li class="page-item"><a class="page-link" href="#">10</a></li>
                <li class="page-item"><a class="page-link" href="#">></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section> 