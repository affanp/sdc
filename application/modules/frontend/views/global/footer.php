    <!-- preloader -->
    <div class="nile-preloader">
        <div class="logo">
            <img src="<?php echo asset_frontend_url(); ?>img/loading-1.svg" alt="">
        </div>
    </div>
    <!-- end preloader -->

    <!-- <a class="go-top box-shadow"><span class="arrow_carrot-up"></span></a> -->
    <!-- <a class="go-wa" href="javascript:;"><img src="<?php echo asset_frontend_url(); ?>img/mobile/Whatsapp.png"></a> -->

    <?php if($this->agent->is_mobile()): ?>
    <div style="height: 7px;background-color: #75eeff;"></div> 
    <footer class="layout-2">

        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo text-center">
                            <img src="<?php echo asset_frontend_url(); ?>img/logo.png" alt="" style="max-width: 30%;">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 text-center">
                        <ul class="social-media mt-3 w-100">
                            <li class="list-inline-item">
                                <a class="facebook" href="#" style="width: 45px;">
                                    <!-- <i class="fab fa-facebook" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/facebook.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="youtube" href="#">
                                    <!-- <i class="fab fa-instagram" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/instagram.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="linkedin" href="#">
                                    <!-- <i class="fab fa-tiktok" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/tiktok.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="twitter" href="#">
                                    <!-- <i class="fab fa-twitter" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/twitter.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="youtube" href="#">
                                    <!-- <i class="fab fa-youtube" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/youtube.png">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="text-center mt-3">
                            <a href="<?php echo site_url("kontak_kami"); ?>" class="text-white contact-mobile">Contact Us</i></a>
                        </div>
                        <ul class="links text-center mt-2" style="padding: 0">
                            <li class="list-inline-item"><a class="text-white" href="https://wingscorp.com">©WingsfoodIndonesia <?php echo date("Y"); ?></a></li>
                            <li class="list-inline-item w-33"><img src="<?php echo asset_frontend_url(); ?>img/wingsfood-sm.png" alt="" class="img-fluid"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <?php else: ?>
    <div style="height: 7px;background-color: #75eeff;"></div>   
    <footer class="layout-2">
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo text-center flex-center">
                            <img src="<?php echo asset_frontend_url(); ?>img/logo.png" alt="" style="max-width: 64%;">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <ul class="social-media flex-center" style="margin-top: 9px;">
                            <?php if(isset($socmed_footer) && !empty($socmed_footer)): echo $socmed_footer; else: ?>
                            <li class="list-inline-item">
                                <a class="facebook" href="#">
                                    <!-- <i class="fab fa-facebook" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/facebook.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="youtube" href="#">
                                    <!-- <i class="fab fa-instagram" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/instagram.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="linkedin" href="#">
                                    <!-- <i class="fab fa-tiktok" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/tiktok.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="twitter" href="#">
                                    <!-- <i class="fab fa-twitter" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/twitter.png">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="youtube" href="#">
                                    <!-- <i class="fab fa-youtube" aria-hidden="true"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/youtube.png">
                                </a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="links text-right flex-center" style="margin-top: 9px;">
                            <li class="list-inline-item mr-5"><a class="contact" href="<?php echo site_url("kontak_kami"); ?>">Contact Us</i></a></li>
                            <li class="list-inline-item mr-5"><a href="https://wingscorp.com">©WingsfoodIndonesia <?php echo date("Y"); ?></a></li>
                            <li class="list-inline-item" style="width: 33%;"><img src="<?php echo asset_frontend_url(); ?>img/wingsfood-sm.png" alt="" class="img-fluid"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </footer>    
    <?php endif; ?>    

    <link href="<?php echo asset_frontend_url(); ?>css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo asset_frontend_url(); ?>css/owl.theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/colors/color-1.min.css">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/elegant_icon.min.css">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/animate.min.css">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/hover-min.css">
    <link href="<?php echo asset_frontend_url(); ?>fontawesome-5.15.3/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_frontend_url(); ?>rslider/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_frontend_url(); ?>rslider/fonts/font-awesome/css/font-awesome.min.css">

    <script src="<?php echo asset_frontend_url(); ?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/scrolla.jquery.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/YouTubePopUp.jquery.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/owl.carousel.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/imagesloaded.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/popper.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/wow.min.js"></script>
    <script src="<?php echo asset_frontend_url(); ?>js/custom.min.js"></script>
</body>

</html>