<!DOCTYPE html>
<html lang="id-ID">

<head>
    <title>Segar Dingin SDC 1000<?php echo isset($title) ? ' - '.$title : ''; ?></title>
    <meta name="author" content="SDC 1000">
    <meta name="robots" content="<?php echo get_meta_robots(); ?>">
    <meta name="googlebot" content="<?php echo get_meta_robots(); ?>">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo asset_frontend_url(); ?>img/logo.png"/>
    <meta name="description" content="Segar Dingin SDC 1000 <?php echo isset($title) ? ' - '.$title : ''; ?>"/>
    <link rel="canonical" href="<?php echo current_url(); ?>" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Segar Dingin SDC 1000 <?php echo isset($title) ? ' - '.$title : ''; ?>" />
    <meta property="og:description" content="Segar Dingin SDC 1000 <?php echo isset($title) ? ' - '.$title : ''; ?>" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />
    <meta property="og:site_name" content="Segar Dingin SDC 1000" />
    <link rel="image_src" href="<?php echo asset_frontend_url('img/logo.png'); ?>"/> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Segar Dingin SDC 1000 <?php echo isset($title) ? ' - '.$title : ''; ?>" />
    <meta name="twitter:title" content="Segar Dingin SDC 1000 <?php echo isset($title) ? ' - '.$title : ''; ?>" />
    <!-- <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Muli:800%7CPoppins:300i,300,400,500,600,700,400i,500%7CPlayfair+Display:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo asset_frontend_url(); ?>css/style.min.css<?php echo rand_num(); ?>">
</head>
<?php if($this->agent->is_mobile()): ?>
<body class="mobile">
<?php else: ?>
<body>
<?php endif; ?>    
    <!--  HEADER  -->
    <header>
        <div class="header-output">
            <div class="container header-in">
                <div class="position-relative">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-12">
                            <a id="logo" href="<?php echo site_url(); ?>" class="d-inline-block flex-center"><img src="<?php echo asset_frontend_url(); ?>img/logo.png" alt="" style="max-width:42% !important;"></a>
                            <a class="mobile-toggle padding-15px border-radius-3" href="#"><i class="fa fa-bars"></i></a>
                        </div>
                        <div class="col-xl-6 col-lg-9 col-md-12 position-inherit">

                            <ul id="menu-main" class="nav-menu float-xl-right text-lg-center link-padding-tb-28px dropdown-light flex-center">
                                <!-- <li <?php if($this->uri->segment(1) == ''){echo 'class="active"';}else{} ?>><a href="<?php echo site_url(); ?>">Home</a></li> -->
                                <li <?php if($this->uri->segment(1) == 'shop'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("shop"); ?>">Where to buy</a></li>
                                <li <?php if($this->uri->segment(1) == 'videos'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("videos"); ?>">Videos</a></li>
                                <li <?php if($this->uri->segment(1) == 'artikel'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("artikel"); ?>">Articles</a></li>
                                <li <?php if($this->uri->segment(1) == 'berita'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("berita"); ?>">News & Events</a></li>
                            </ul>

                        </div>
                        <div class="col-lg-3 col-md-12  d-none d-lg-block text-right">
                            <?php if(isset($socmed_header) && !empty($socmed_header)): echo $socmed_header; else: ?>
                            <div class="d-none flex-center d-xl-block pull-right model-link">
                                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                                    <!-- <i class="fab fa-youtube"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/youtube.png">
                                </a>
                            </div>
                            <div class="d-none flex-center d-xl-block pull-right model-link mr-1">
                                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                                    <!-- <i class="fab fa-twitter"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/twitter.png">
                                </a>
                            </div>
                            <div class="d-none flex-center d-xl-block pull-right model-link mr-1">
                                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                                    <!-- <i class="fab fa-tiktok"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/tiktok.png">
                                </a>
                            </div>
                            <div class="d-none flex-center d-xl-block pull-right model-link mr-1">
                                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                                    <!-- <i class="fab fa-instagram"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/instagram.png">
                                </a>
                            </div>
                            <div class="d-none flex-center d-xl-block pull-right model-link mr-1">
                                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                                    <!-- <i class="fab fa-facebook"></i> -->
                                    <img class="social-media-icon" src="<?php echo asset_frontend_url(); ?>img/facebook.png">
                                </a>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="sidebar-menu">
        <div class="d-flex justify-content-between">
            <div class="p-3">
                <img src="<?php echo asset_frontend_url(); ?>img/logo.png" alt="" style="max-width:56% !important;">
            </div>
            <div class="p-3">
                <div class="close-container" id="mobile-toggle-close">
                  <div class="leftright"></div>
                  <div class="rightleft"></div>
                </div>
            </div>
        </div>
        <div class="sidebar-menu-content">
            <ul id="menu-mobile" class="nav-menu float-xl-right text-lg-center link-padding-tb-28px">
                <!-- <li <?php if($this->uri->segment(1) == ''){echo 'class="active"';}else{} ?>><a href="<?php echo site_url(); ?>">Home</a></li> -->
                <li <?php if($this->uri->segment(1) == 'shop'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("shop"); ?>">Where to buy</a></li>
                <li <?php if($this->uri->segment(1) == 'videos'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("videos"); ?>">Videos</a></li>
                <li <?php if($this->uri->segment(1) == 'artikel'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("artikel"); ?>">Articles</a></li>
                <li <?php if($this->uri->segment(1) == 'berita'){echo 'class="active"';}else{} ?>><a href="<?php echo site_url("berita"); ?>">News & Events</a></li>
            </ul>
        </div>
    </div>
    <!-- // HEADER  -->