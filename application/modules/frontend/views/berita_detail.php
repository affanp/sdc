<style>
    #readMore:hover{
        color: #F37B2F;
    }
</style>
<section id="blog" class="padding-tb-100px blog-detail-bg position-relative">
    <img src="<?php echo asset_frontend_url('img/bg-detail-artikel-top.png'); ?>" class="blog-detail-bg-top">
    <div class="container mh-1700">
        <div class="row">
        	<div class="col-12 text-center">
                <?php if($res_news_events['slug'] == 'tes-artikel'): ?>
                <div class="bg-white shadow-orange border-radius-15"> 
                    <img src="<?php echo $res_news_events['image']; ?>"> 
                </div>
        		<?php elseif(!empty($res_news_events['image']) && file_exists(FCPATH.'assets/backend/uploads/newsEvents/'.$res_news_events['image'])): ?>
                <div class="bg-white shadow-orange border-radius-15"> 
        		    <img src="<?php echo asset_backend_url('uploads/newsEvents/'.$res_news_events['image']); ?>">
                </div>    
        		<?php else: ?>
        		<img src="<?php echo asset_frontend_url('img/empty-artikel.png'); ?>">	
        		<?php endif; ?> 
        	</div>
        </div>

        <div class="row mt-5">
        	<div class="col-md-8 col-sm-12 blog-detail mb-5">
        		<p class="date"><?php echo date('d M Y', strtotime($res_news_events['created_at'])); ?></p>
        		<h1 class="title" style="font-size: 2rem;"><?php echo $res_news_events['title']; ?></h1>
                <div class="description mt-5">
                    <?php echo $res_news_events['description']; ?>
                </div>
        	</div>
        	<div class="col-md-4 col-sm-12">
        		<div class="nile-title layout-1 text-center">
                    <div class="position-relative">
                        <img src="<?php echo asset_frontend_url();?>img/mobile/Brush Hijau.png" alt="Berita Lainnya" style="width:100%;">
                        <?php if($this->agent->is_mobile()): ?>
                        <div class="centered title-w-bg-white title-medium">BERITA LAINNYA</div>    
                        <?php else: ?>    
                        <div class="centered title-w-bg-white title-xlarge">BERITA LAINNYA</div>
                        <?php endif; ?>
                    </div> 
                </div>

                <div class="row">
                <?php if(!empty($res_news_events_other)): foreach($res_news_events_other as $rneo): ?>
                <div class="col-12">
	                <div class="fizo-blog layout-2 shadow-orange">
	                    <div class="img-in">
	                        <?php if(!empty($rneo['image'])):?>
	                        <img src="<?php echo file_exists(FCPATH.'assets/backend/uploads/newsEvents/'.$rneo['image']) ? asset_backend_url('uploads/newsEvents/'.$rneo['image']) : asset_frontend_url('img/tes/600x450.png'); ?>" alt="<?php echo $rneo['title']; ?>">
	                        <?php else: ?>
	                        <img src="<?php echo asset_frontend_url('img/tes/600x450.png'); ?>" alt="">
	                        <?php endif; ?>    
	                        <!-- <a href="<?php echo site_url('berita/detail/'.$rneo['slug']); ?>" class="read-more flex-center">Read More</a> -->
	                    </div>
	                    <a href="<?php echo site_url('berita/detail/'.$rneo['slug']); ?>" class="title_in ml-3 mr-3 pb-3"><?php echo $rneo['title']; ?></a>
	                    <a href="<?php echo site_url('berita/detail/'.$rneo['slug']); ?>" class="read-more pb-3 pr-4" id="readMore">Baca Selengkapnya</a>
	                </div>
	            </div>	
                <?php endforeach; endif; ?>	
                </div>	
        	</div>
        </div>
    </div>
    <img src="<?php echo asset_frontend_url('img/bg-detail-artikel-bottom.png'); ?>" class="blog-detail-bg-bottom">
</section>        	