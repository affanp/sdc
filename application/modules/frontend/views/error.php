<section class="padding-top-120px padding-bottom-90px" id="404">
    <div class="container">
        <div class="row content">
            <div class="col-md-6">
                <h3>404</h3>

                <p class="text-justify mb-5">Halaman tidak ditemukan.</p>

                <a href="<?php echo site_url(); ?>" class="btn-orange mt-5">KEMBALI</a>
            </div>   
        </div>    
    </div>
</section>