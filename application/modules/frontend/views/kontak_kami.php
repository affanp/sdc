<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/banner.png)" class="padding-top-120px padding-bottom-90px" id="shop" >
    
</section>
<section class="padding-bottom-50px padding-top-100px">
    <div class="container">
        <div class="map-layout margin-bottom-75px">
            <iframe src="https://maps.google.com/maps?q=Jl.%20Alex%20Bangun,%20Asih,%20Jati,%20RT.004/RW.002,%20Margahayu,%20Bekasi%20Tim.,%20Kota%20Bks,%20Jawa%20Barat%2017113&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="row">

            <div class="col-lg-4">
                <div class="fizo-icon-box layout-2">
                    <div class="icon"><img src="<?php echo asset_frontend_url(); ?>icons/map-location.svg" alt=" "></div>
                    <h3>London</h3>
                    <div class="dis">USA, New York<br> Mondella street 34, buld 8</div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-icon-box layout-2">
                    <div class="icon"><img src="<?php echo asset_frontend_url(); ?>icons/www.svg" alt=" "></div>
                    <h3>www.yoursite.com</h3>
                    <div class="dis">USA, New York<br> www.yoursite.com</div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="fizo-icon-box layout-2">
                    <div class="icon"><img src="<?php echo asset_frontend_url(); ?>icons/24-hours.svg" alt=" "></div>
                    <h3>+222 123 333 019</h3>
                    <div class="dis">USA, New York<br> Mondella street 34, buld 8</div>
                </div>
            </div>

        </div>
    </div>
</section>