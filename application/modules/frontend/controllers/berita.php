<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model("FrontendModel", "fm");
		$this->socmed_data = $this->fm->get_socmed();
	}

	public function index() 
	{
		$data = $header_data = $footer_data = array();

		$header_data['title'] = 'News & Events';
		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];

		if($this->uri->segment(2)){
			$prm['start'] = $this->uri->segment(2);
		}
		
		$prm['table'] = 'news_events';
		if($this->agent->is_mobile()){
			$prm['limit'] = $limit = 6;
		}else{
			$prm['limit'] = $limit = 9;	
		}
		$prm['order_by'] = 'id';
		$data['res_news_events'] = $this->fm->get_data($prm)->result_array();
		unset($prm['limit']);
		$res_news_events = $this->fm->get_data($prm,true)->row_array();

		$this->load->library('pagination');

		$config['base_url'] = site_url($this->router->fetch_class());;
		$config['total_rows'] = isset($res_news_events['cnt'])?$res_news_events['cnt']:0;
		$config['per_page'] = $limit;
		$config['uri_segment'] = 2; 
		$config['attributes']['rel'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$this->load->view('global/header', $header_data);
		$this->load->view('berita', $data);
		$this->load->view('global/footer', $footer_data);
	}

	public function detail($slug)
	{
		$data = $header_data = $footer_data = array();

		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];

		if($slug == 'tes-artikel'){
			$header_data['title'] = 'News & Events - Tes Artikel';

			$html = '<h2>What is Lorem Ipsum?</h2>
			<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			<p><img src="'.asset_frontend_url('img/tes/sdc1.jpg').'" alt="" /></p>
			<h2>Why do we use it?</h2>
			<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using "Content here, content here", making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>';

			$data['res_news_events'] = array(
				'id' => 1,
				'image' => asset_frontend_url('img/tes/600x450.png'),
				'title' => 'Tes Artikel',
				'description' => $html,
				'created_at' => '2021-07-08 16:15:09',
				'slug' => 'tes-artikel'
			);

			$data['res_news_events_other'] = array(
				array(
					'id' => 2,
					'image' => asset_frontend_url('img/tes/600x450.png'),
					'title' => 'Tes Artikel 2',
					'description' => $html,
					'created_at' => '2021-07-08 16:15:09',
					'slug' => 'tes-artikel'
				),
				array(
					'id' => 2,
					'image' => asset_frontend_url('img/tes/600x450.png'),
					'title' => 'Tes Artikel 3',
					'description' => $html,
					'created_at' => '2021-07-08 16:15:09',
					'slug' => 'tes-artikel'
				),
			);
		}else{
			$prm['table'] = 'news_events';
			$prm['where'] = array('lower(slug)' => strtolower($slug));
			$data['res_news_events'] = $this->fm->get_data($prm)->row_array();

			if(empty($data['res_news_events'])){
				redirect(site_url('not_found'));
			}

			$header_data['title'] = 'News & Events - '.$data['res_news_events']['title'];

			$prm2['table'] = 'news_events';
			$prm2['limit'] = 2;
			$prm2['where'] = array("id != ".$data['res_news_events']['id'] => NULL);
			$prm2['order_by'] = 'id';
			$data['res_news_events_other'] = $this->fm->get_data($prm2)->result_array();
		}	

		$this->load->view('global/header', $header_data);
		$this->load->view('berita_detail', $data);
		$this->load->view('global/footer', $footer_data);
	}
}
