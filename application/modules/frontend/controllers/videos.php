<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Videos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model("FrontendModel", "fm");
		$this->socmed_data = $this->fm->get_socmed();
	}
 
	public function index() {
		$data = $header_data = $footer_data = array();

		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];

		$header_data['title'] = ucwords($this->router->fetch_class());

		if($this->uri->segment(2)){
			$prm['start'] = $this->uri->segment(2);
		}
		
		$prm['table'] = 'videos';
		if($this->agent->is_mobile()){
			$prm['limit'] = $limit = 6;
		}else{
			$prm['limit'] = $limit = 9;	
		}
		$prm['order_by'] = 'id';
		$data['res_videos'] = $this->fm->get_data($prm)->result_array();
		unset($prm['limit']);
		$res_videos = $this->fm->get_data($prm,true)->row_array();

		$this->load->library('pagination');

		$config['base_url'] = site_url($this->router->fetch_class());;
		$config['total_rows'] = isset($res_videos['cnt'])?$res_videos['cnt']:0;
		$config['per_page'] = $limit;
		$config['uri_segment'] = 2; 
		$config['attributes']['rel'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		
		$this->load->view('global/header', $header_data);
		$this->load->view('videos', $data);
		$this->load->view('global/footer', $footer_data);
	}
}
