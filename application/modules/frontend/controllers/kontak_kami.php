<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Kontak_kami extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model("FrontendModel", "fm");
		$this->socmed_data = $this->fm->get_socmed();
	}
 
	public function index() {
		$data = $header_data = $footer_data = array();

		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];

		$header_data['title'] = 'Kontak Kami';
		
		$this->load->view('global/header', $header_data);
		$this->load->view('kontak_kami', $data);
		$this->load->view('global/footer', $footer_data);
	}
}
