<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Frontend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
        $this->load->model("FrontendModel", "fm");
        $this->socmed_data = $this->fm->get_socmed();
	}
 
	public function index() {
		$data = $header_data = $footer_data = array();

		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];

        $data['res_slider'] = $this->fm->get_data(array('table' => 'sliders', 'limit' => 1, 'order_by' => 'id', 'where' => array('status' => 1)))->result_array();
        $data['res_articles'] = $this->fm->get_data(array('table' => 'articles', 'limit' => 3, 'where' => array('status' => 1), 'order_by' => 'id'))->result_array();
        $data['res_news_events'] = $this->fm->get_data(array('table' => 'news_events', 'limit' => 3, 'order_by' => 'id'))->result_array();
		
		if($this->agent->is_mobile()){
			$this->load->view('global/header', $header_data);
			$this->load->view('home_mobile', $data);
			$this->load->view('global/footer', $footer_data);
		}else{
			$this->load->view('global/header', $header_data);
			$this->load->view('home', $data);
			$this->load->view('global/footer', $footer_data);
		}
	}

	public function not_found() {
		$data = $header_data = $footer_data = array();

		$header_data['socmed_header'] = $this->socmed_data['socmed_header'];
		$footer_data['socmed_footer'] = $this->socmed_data['socmed_footer'];
		
		$this->load->view('global/header', $header_data);
		$this->load->view('error', $data);
		$this->load->view('global/footer', $footer_data);
	}
}
