<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class FrontendModel extends CI_Model {

    public function get_data($prm=array(), $count=false) {

    	if($count){
            $this->db->select('count(*) as cnt');
        }elseif(isset($prm['select'])){
    		$this->db->select($prm['select']);
    	}else{
    		$this->db->select('*');
    	}
        
        $this->db->from($prm['table']);

        if(isset($prm['where'])){
        	$this->db->where($prm['where']);
        }	

        if(isset($prm['limit'])){
        	if(isset($prm['start'])){
        		$this->db->limit($prm['limit'], $prm['start']);
        	}else{
        		$this->db->limit($prm['limit']);
        	}
        }

        if(isset($prm['order_by'])){
        	if(isset($prm['order_dir'])){
        		$this->db->order_by($prm['order_by'], $prm['order_dir']);
        	}else{
        		$this->db->order_by($prm['order_by'], "desc");
        	}
        }

        $query = $this->db->get();

        return $query;

    }

    public function get_socmed() {
        $this->db->select('*');
        $this->db->from('social_media');
        $this->db->where(array('status' => 1));
        $query = $this->db->get();
        $res = $query->result_array();

        $html_footer = $html_header = '';

        if(!empty($res)){
            $res_new = $arr = array();
            foreach ($res as $value) {
                $key = strtolower($value['name']);
                $res_new[$key] = $value;
                $arr[] = $key;
            }

            $html_footer = $this->sort_socmed_footer($res_new, $arr);
            $html_header = $this->sort_socmed_header($res_new, $arr);
        }

        return array('socmed_footer' => $html_footer, 'socmed_header' => $html_header);
    }

    private function sort_socmed_footer($res_new, $arr){
        $html_footer = '';

        if(in_array('facebook', $arr)){
            $link = isset($res_new['facebook']['link'])?$res_new['facebook']['link']:'#';
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="facebook" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/facebook.png').'">
                </a>';
            $html_footer .= '</li>';
        }else{
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="facebook" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/facebook.png').'">
                </a>';
            $html_footer .= '</li>';
        }

        if(in_array('instagram', $arr)){
            $link = isset($res_new['instagram']['link'])?$res_new['instagram']['link']:'#';
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="instagram" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/instagram.png').'">
                </a>';
            $html_footer .= '</li>';
        }else{
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="instagram" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/instagram.png').'">
                </a>';
            $html_footer .= '</li>';
        }

        if(in_array('tiktok', $arr)){
            $link = isset($res_new['tiktok']['link'])?$res_new['tiktok']['link']:'#';
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="tiktok" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/tiktok.png').'">
                </a>';
            $html_footer .= '</li>';
        }else{
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="tiktok" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/tiktok.png').'">
                </a>';
            $html_footer .= '</li>';
        }

        if(in_array('twitter', $arr)){
            $link = isset($res_new['twitter']['link'])?$res_new['twitter']['link']:'#';
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="twitter" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/twitter.png').'">
                </a>';
            $html_footer .= '</li>';
        }else{
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="twitter" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/twitter.png').'">
                </a>';
            $html_footer .= '</li>';
        }

        if(in_array('youtube', $arr)){
            $link = isset($res_new['youtube']['link'])?$res_new['youtube']['link']:'#';
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="youtube" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/youtube.png').'">
                </a>';
            $html_footer .= '</li>';
        }else{
            $html_footer .= '<li class="list-inline-item">';
            $html_footer .= '<a class="youtube" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/youtube.png').'">
                </a>';
            $html_footer .= '</li>';
        }

        return $html_footer;
    }

    private function sort_socmed_header($res_new, $arr){
        $html_header = '';

        if(in_array('youtube', $arr)){
            $link = isset($res_new['youtube']['link'])?$res_new['youtube']['link']:'#';
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/youtube.png').'">
                </a>
            </div>';
        }else{
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/youtube.png').'">
                </a>
            </div>';
        }

        if(in_array('twitter', $arr)){
            $link = isset($res_new['twitter']['link'])?$res_new['twitter']['link']:'#';
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/twitter.png').'">
                </a>
            </div>';
        }else{
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/twitter.png').'">
                </a>
            </div>';
        }

        if(in_array('tiktok', $arr)){
            $link = isset($res_new['tiktok']['link'])?$res_new['tiktok']['link']:'#';
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/tiktok.png').'">
                </a>
            </div>';
        }else{
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/tiktok.png').'">
                </a>
            </div>';
        }

        if(in_array('instagram', $arr)){
            $link = isset($res_new['instagram']['link'])?$res_new['instagram']['link']:'#';
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/instagram.png').'">
                </a>
            </div>';
        }else{
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/instagram.png').'">
                </a>
            </div>';
        }

        if(in_array('facebook', $arr)){
            $link = isset($res_new['facebook']['link'])?$res_new['facebook']['link']:'#';
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="'.$link.'">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/facebook.png').'">
                </a>
            </div>';
        }else{
            $html_header .= '<div class="d-none flex-center d-xl-block pull-right model-link">
                <a class="ba-1 model-link text-dark opacity-hover-8" href="#">
                    <img class="social-media-icon" src="'.asset_frontend_url('img/facebook.png').'">
                </a>
            </div>';
        }

        return $html_header;
    }
 }
