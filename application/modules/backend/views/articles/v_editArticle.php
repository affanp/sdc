<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<style>
  .title-table{
    text-align: center;
  }
</style>
<?php backendHeader(); ?>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>Update Article Form</h1>
          </div>
          <?php if(isset($this->session->userdata['statusUpdateArticle'])) { ?>
            <div class="card-body" id="alertMessage">
              <?php 
                $statusUpdateArticle = $this->session->userdata['statusUpdateArticle'];
                if($statusUpdateArticle['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusUpdateArticle['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusUpdateArticle['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
          <?php if(isset($this->session->userdata['statusHapusArticle'])) { ?>
            <div class="card-body" id="alertMessageHapus">
              <?php 
                $statusHapusArticle = $this->session->userdata['statusHapusArticle'];
                if($statusHapusArticle['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusHapusArticle['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusHapusArticle['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>backend/articles/updateArticle">
                <div class="card-body">
                  <div class="form-group">
                    <label for="labelImage">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="hidden" name="id" value="<?php echo $detailArticle['id']; ?>" />
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" onchange="loadFile(event)">
                        <label class="custom-file-label" for="labelChooseFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    <img id="output" style="max-width: 200px; margin-top: 20px;"/>
                    <?php if($detailArticle['image']) { ?>
                      <img id="output" style="max-width: 200px; margin-top: 20px;" src="<?php echo base_url(); ?>assets/backend/uploads/article/<?php echo $detailArticle['image']; ?>"/>
                    <?php } ?>
                  </div>
                  <div class="form-group">
                    <label for="labelTitle">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Link" value="<?php echo $detailArticle['title'];?>">
                  </div>
                  <div class="form-group">
                    <label for="labelDescription">Description</label>
                    <textarea id="summernote" rows="3" name="description">
                      <?php echo $detailArticle['description']; ?>
                    </textarea>
                  </div>
                  <div class="form-check">
                    <?php if($detailArticle['status']) { ?>
                      <?php if($detailArticle['status'] == 1) { ?>
                        <input type="checkbox" class="form-check-input" id="active" name="status" checked>
                      <?php } else { ?>
                        <input type="checkbox" class="form-check-input" id="active" name="status">
                      <?php } ?>
                    <?php } ?>
                    <label class="form-check-label" for="labelActive">Active</label>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table Articles</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th class="title-table">Image</th>
                    <th class="title-table">Title</th>
                    <th class="title-table">Status</th>
                    <th class="title-table">Edit</th>
                    <?php if($sessions['userType'] == 1) { ?>
                      <th class="title-table">Delete</th>
                    <?php } ?>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dataArticle as $data) { ?>
                      <tr>
                        <td style="text-align: center;"><img src="http://localhost/sdc/assets/backend/uploads/article/<?php echo $data['image']; ?>" style="max-width: 100px;" /></td>
                        <td><?php echo $data['title']; ?></td>
                        <td style="text-align: center;">
                          <?php if($data['status'] == 1) { ?>
                            Active
                          <?php } else { ?>
                            Inactive
                          <?php } ?>
                        </td>
                        <td style="text-align: center;"><a href="<?php echo base_url(); ?>backend/articles/editArticle/<?php echo $data['id']; ?>" class="btn btn-warning">EDIT</a></td>
                        <?php if($sessions['userType'] == 1) { ?>
                          <td style="text-align: center;"><a href="#" class="btn btn-danger" data-toggle="modal" onClick="deleteAction(<?php echo $data['id']; ?>)"  data-target="#modal-xl">DELETE</a></td>
                        <?php } ?>
                          <div class="modal fade" id="modal-xl">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Apakah Anda Yakin?</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body" id="contentPopup"></div>
                                <div class="modal-footer justify-content-between">
                                  <a href="<?php echo base_url(); ?>backend/articles/removeArticle/edit/<?php echo $data['id']; ?>/<?php echo $detailArticle['id']; ?>" class="btn btn-default">Yakin</a>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal">CANCEL</button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php backendFooter(); ?>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src)
      }
    };
    $('#alertMessage').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/articles/removeStatusUpdateArticle",
        type: "post"
     });
    });
    $('#alertMessageHapus').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/articles/removeStatusHapusArticle",
        type: "post"
     });
    });
</script>
<script>
  $(function () {
    // Summernote
    $('#summernote').summernote({
      height: 300
    })

    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
      mode: "htmlmixed",
      theme: "monokai",
    });
  })
  function deleteAction(id) {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/articles/getDataDelete",
        type: "post",
        dataType: "json",
        data: {id: id},
        success: function(data) {
          let title = data.title;
          document.getElementById('contentPopup').innerHTML = "Apakah Anda Yakin ingin Menghapus Marketplace dengan title <b>"+title+"</b> ini";
        }
     });
    }
</script>