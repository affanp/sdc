<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<?php backendHeader(); ?>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>Update News and Events Form</h1>
          </div>
          <?php if(isset($this->session->userdata['statusUpdateNewsEvents'])) { ?>
            <div class="card-body" id="alertMessage">
              <?php 
                $statusUpdateNewsEvents = $this->session->userdata['statusUpdateNewsEvents'];
                if($statusUpdateNewsEvents['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusUpdateNewsEvents['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusUpdateNewsEvents['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
          <?php if(isset($this->session->userdata['statusHapusNewsEvents'])) { ?>
            <div class="card-body" id="alertMessageHapus">
              <?php 
                $statusHapusNewsEvents = $this->session->userdata['statusHapusNewsEvents'];
                if($statusHapusNewsEvents['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusHapusNewsEvents['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusHapusNewsEvents['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>backend/newsEvents/updateNewsEvents">
                <div class="card-body">
                  <div class="form-group">
                    <label for="labelDescription">Category</label>
                    <input type="hidden" name="id" value="<?php echo $detailNewsEvents['id']; ?>" />
                    <select name="category" class="form-control">
                      <option value=""></option>
                        <?php foreach($dataCategories as $data) { ?>
                          <?php if($detailNewsEvents['category'] == $data['id']) { ?>
                            <option value="<?php echo $data['id']; ?>" selected><?php echo $data['name']; ?></option>
                          <?php } else { ?>
                            <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
                          <?php } ?>
                        <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="labelImage">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="labelChooseFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    <img id="output" style="max-width: 200px; margin-top: 20px;"/>
                    <?php if ($detailNewsEvents['image']) { ?>
                      <img id="output" style="max-width: 200px; margin-top: 20px;" src="<?php echo base_url(); ?>assets/backend/uploads/newsEvents/<?php echo $detailNewsEvents['image']; ?>"/>
                    <?php } ?>
                  </div>
                  <div class="form-group">
                    <label for="labelTitle">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Link" value="<?php echo $detailNewsEvents['title']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="labelDescription">Description</label>
                    <textarea id="summernote" rows="3" name="description"><?php echo $detailNewsEvents['description']; ?></textarea>
                  </div>
                  <div class="form-check">
                    <?php if($detailNewsEvents['status'] == 1) { ?>
                      <input type="checkbox" class="form-check-input" id="active" name="status" checked>
                    <?php } else { ?>
                      <input type="checkbox" class="form-check-input" id="active" name="status">
                    <?php } ?>
                    <label class="form-check-label" for="labelActive">Active</label>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table News and Events</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th class="title-table">Category</th>
                    <th class="title-table">Image</th>
                    <th class="title-table">Title</th>
                    <th class="title-table">Status</th>
                    <th class="title-table">Edit</th>
                    <?php if($sessions['userType'] == 1) { ?>
                      <th class="title-table">Delete</th>
                    <?php } ?>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dataNewsEvents as $data) { ?>
                      <tr>
                        <td style="text-align: center;"><?php echo $data['nameCategory']; ?></td>
                        <td style="text-align: center;"><img src="http://localhost/sdc/assets/backend/uploads/newsEvents/<?php echo $data['imageNewsEvents']; ?>" style="max-width: 100px;" /></td>
                        <td><?php echo $data['titleNewsEvents']; ?></td>
                        <td style="text-align: center;">
                          <?php if($data['statusNewsEvents'] == 1) { ?>
                            Active
                          <?php } else { ?>
                            Inactive
                          <?php } ?>
                        </td>
                        <td style="text-align: center;"><a href="<?php echo base_url(); ?>backend/newsEvents/editNewsEvents/<?php echo $data['idNewsEvents']; ?>" class="btn btn-warning">EDIT</a></td>
                        <?php if($sessions['userType'] == 1) { ?>
                          <td style="text-align: center;"><a href="#" class="btn btn-danger" onClick="deleteAction(<?php echo $data['idNewsEvents']; ?>)" data-toggle="modal" data-target="#modal-xl">DELETE</a></td>
                        <?php } ?>
                        <div class="modal fade" id="modal-xl">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Apakah Anda Yakin?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body" id="contentPopup"></div>
                              <div class="modal-footer justify-content-between">
                                <a href="<?php echo base_url(); ?>backend/newsEvents/removeNewsEvents/edit/<?php echo $data['idNewsEvents']; ?>/<?php echo $detailNewsEvents['id']; ?>" class="btn btn-default">Yakin</a>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">CANCEL</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php backendFooter(); ?>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src)
      }
    };
    $('#alertMessage').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/newsEvents/removeStatusUpdateNewsEvents",
        type: "post"
     });
    });
    $('#alertMessageHapus').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/newsEvents/removeStatusHapusNewsEvents",
        type: "post"
     });
    });
    function deleteAction(id) {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/newsEvents/getDataDelete",
        type: "post",
        dataType: "json",
        data: {id: id},
        success: function(data) {
          let title = data.title;
          document.getElementById('contentPopup').innerHTML = "Apakah Anda Yakin ingin Menghapus News or Events dengan title <b>"+title+"</b> ini";
        }
     });
    }
</script>
<script>
  $(function () {
    // Summernote
    $('#summernote').summernote({
      height: 300
    })

    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
      mode: "htmlmixed",
      theme: "monokai",
    });
  })
</script>