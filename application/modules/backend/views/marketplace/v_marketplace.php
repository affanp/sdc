<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<style>
  .title-table{
    text-align: center;
  }
</style>
<?php backendHeader(); ?>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>Marketplace Form</h1>
          </div>
          <?php if(isset($this->session->userdata['statusSaveMarketplace'])) { ?>
            <div class="card-body" id="alertMessage">
              <?php 
                $statusSaveMarketplace = $this->session->userdata['statusSaveMarketplace'];
                if($statusSaveMarketplace['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusSaveMarketplace['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusSaveMarketplace['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
          <?php if(isset($this->session->userdata['statusHapusMarketplace'])) { ?>
            <div class="card-body" id="alertMessageHapus">
              <?php 
                $statusHapusMarketplace = $this->session->userdata['statusHapusMarketplace'];
                if($statusHapusMarketplace['success']) { ?>
                  <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    <?php echo $statusHapusMarketplace['message']; ?>
                  </div>
                <?php } else { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    <?php echo $statusHapusMarketplace['message']; ?>
                  </div>
                <?php } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
            <form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>backend/marketplace/saveMarketplace">
                <div class="card-body">
                  <div class="form-group">
                    <label for="labelLogo">Logo</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="labelChooseFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    <img id="output" style="max-width: 200px; margin-top: 20px;"/>
                  </div>
                  <div class="form-group">
                    <label for="labelLink">Link</label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Enter Link">
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="active" name="status">
                    <label class="form-check-label" for="labelActive">Active</label>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table Marketplace</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th class="title-table">Id</th>
                    <th class="title-table">Image</th>
                    <th class="title-table">Link</th>
                    <th class="title-table">Status</th>
                    <th class="title-table">Edit</th>
                    <?php if($sessions['userType'] == 1) { ?>
                      <th class="title-table">Delete</th>
                    <?php } ?>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($dataMarketplace as $data) { ?>
                    <tr>
                      <td style="text-align: center;"><?php echo $data['idMarketplace']; ?>
                      <td style="text-align: center;"><img src="http://localhost/sdc/assets/backend/uploads/marketplace/<?php echo $data['logoMarketplace']; ?>" style="max-width: 100px;" /></td>
                      <td><a href="<?php echo $data['linkMarketplace']; ?>" target="_blank"><?php echo $data['linkMarketplace']; ?></a></td>
                      <td style="text-align: center;"><?php echo $data['statusName']; ?></td>
                      <td style="text-align: center;"><a href="<?php echo base_url(); ?>backend/marketplace/editMarketPlace/<?php echo $data['idMarketplace']; ?>" class="btn btn-warning">EDIT</a></td>
                      <?php if($sessions['userType'] == 1) { ?>
                        <td style="text-align: center;"><a href="#" onClick="deleteAction(<?php echo $data['idMarketplace']; ?>)"  class="btn btn-danger" data-toggle="modal" data-target="#modal-xl">DELETE</a></td>
                      <?php } ?>
                      <div class="modal fade" id="modal-xl">
                        <div class="modal-dialog modal-xl">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Apakah Anda Yakin?</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" id="contentPopup"></div>
                            <div class="modal-footer justify-content-between">
                              <a href="<?php echo base_url(); ?>backend/marketplace/removeMarketplace/new/<?php echo $data['idMarketplace']; ?>" class="btn btn-default">Yakin</a>
                              <button type="button" class="btn btn-primary" data-dismiss="modal">CANCEL</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </tr>
                  <?php } ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php backendFooter(); ?>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script> 
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src)
      }
    };
    $('#alertMessage').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/marketplace/removeStatusSaveMarketplace",
        type: "post"
     });
    });
    $('#alertMessageHapus').delay(3000).fadeOut('slow', function() {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/marketplace/removeStatusHapusMarketplace",
        type: "post"
     });
    });
    function deleteAction(id) {
      $.ajax({
        url: "<?php echo base_url() ?>/backend/marketplace/getDataDelete",
        type: "post",
        dataType: "json",
        data: {id: id},
        success: function(data) {
          let id = data.id;
          document.getElementById('contentPopup').innerHTML = "Apakah Anda Yakin ingin Menghapus Marketplace dengan id <b>"+id+"</b> ini";
        }
     });
    }
  </script>
  