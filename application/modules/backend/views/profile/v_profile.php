<?php backendHeader(); ?>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>Profile Form</h1>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>backend/profile/updateUser">
                <div class="card-body">
                  <div class="form-group">
                    <label for="labelImage">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="hidden" name="id" value="<?php echo $dataUser['id']; ?>">
                        <input type="file" class="custom-file-input" id="image" name="image">
                        <label class="custom-file-label" for="labelChooseFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    <?php if($dataUser['image']) { ?>
                      <img src="<?php echo base_url(); ?>assets/backend/uploads/user/<?php echo $dataUser['image']; ?>" style="max-width: 200px;">
                    <?php } ?>
                  </div>
                  <div class="form-group">
                    <label for="labelName">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="<?php echo $dataUser['name']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="labelEmail">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="<?php echo $dataUser['email']; ?>">
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-xl">
                        Change Password
                    </button>
                </div>
              </form>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
<form method="POST" action="<?php echo base_url(); ?>backend/profile/updateProfile">
  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Change Password</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $dataUser['id']; ?>" >
            <label for="labelOldPassword">Old Password</label>
            <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="*****">
          </div>
          <div class="form-group">
            <label for="labelNewPassword">New Password</label>
            <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="*****">
          </div>
          <div class="form-group">
            <label for="labelReTypePassword">Re-Type Password</label>
            <input type="password" class="form-control" id="reTYpePassword" name="confirmPassword" placeholder="*****">
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" value="Save Changes"  class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</form>
<?php backendFooter(); ?>
  