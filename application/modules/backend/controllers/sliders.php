
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Sliders extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session', 'upload'));
		$this->load->model('sliderModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}

	public function index() {
		$sessions = $this->session->all_userdata();
		$dataSlider = $this->sliderModel->getDataSlider();
		$this->load->view('sliders/v_sliders', array(
			'dataSlider' => $dataSlider,
			'sessions' => $sessions
		));
	}
	
	public function saveSlider() {
		$success = true;
		$data = [];
		$imageName = $_FILES['image']['name'];
		$imageMobile = $_FILES['imageMobile']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');	
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/logo/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $imageName;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$config['upload_path']          = './assets/backend/uploads/logo/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $imageMobile;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('imageMobile')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$imageName = str_replace(" ", "_", $imageName);
				$imageMobile = str_replace(" ", "_", $imageMobile);
				$dataSave = array(
					'image' => $imageName,
					'imageMobile' => $imageMobile,
					'title' => $title,
					'description' => $description,
					'link' => $link,
					'status' => $status
				);
				$saveId = $this->sliderModel->saveSlider($dataSave);
				if($saveId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Logo Baru telah berhasil di simpan";
			$this->session->set_userdata('statusSaveSlider', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Logo Baru telah gagal di simpan";
			$this->session->set_userdata('statusSaveSlider', $data);
		}
		redirect('/backend/sliders', 'refresh');
	}

	public function removeStatusSaveSlider() {
		$this->session->unset_userdata('statusSaveSlider');
	}

	public function editSlider() {
		$slidersId = $this->uri->segment(4);
		$detailSliders = $this->sliderModel->getDetailSlider($slidersId);
		$dataSlider = $this->sliderModel->getDataSliderEdit($slidersId);
		$sessions = $this->session->all_userdata();
		$this->load->view('sliders/v_editSlider', array(
			'detailSliders' => $detailSliders,
			'dataSlider' => $dataSlider,
			'sessions' => $sessions
		));
	}

	public function updateSlider() {
		$success = true;
		$data = [];
		$slidersId = $this->input->post('slidersId');
		$imageName = $_FILES['image']['name'];
		$imageMobile = $_FILES['imageMobile']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');	
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($imageName) {
			$config['upload_path']          = './assets/backend/uploads/logo/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $imageName;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				if($imageMobile) {
					$config['upload_path']          = './assets/backend/uploads/logo/';
					$config['allowed_types']        = 'jpg|png|jpeg';
					$config['file_name']			= $imageMobile;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('imageMobile')){
						$error = array('error' => $this->upload->display_errors());
						print_r($error['error']);
						$success = false;
					} else {
						$dataUpdate = array(
							'id' => $slidersId,
							'image' => $imageName,
							'imageMobile' => $imageMobile,
							'title' => $title,
							'description' => $description,
							'link' => $link,
							'status' => $status
						);
						$update = $this->sliderModel->updateSliderWithImages($dataUpdate);
						if($update) {
							$success = true;
						} else {
							$success = false;
						}
					}
				} else {
					$dataUpdate = array(
						'id' => $slidersId,
						'image' => $imageName,
						'title' => $title,
						'description' => $description,
						'link' => $link,
						'status' => $status
					);
					$update = $this->sliderModel->updateSliderWIthImage($dataUpdate);
					if($update) {
						$success = true;
					} else {
						$success = false;
					}
				}
			}
		} else {
			if($imageMobile) {
				$config['upload_path']          = './assets/backend/uploads/logo/';
				$config['allowed_types']        = 'jpg|png|jpeg';
				$config['file_name']			= $imageMobile;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('imageMobile')){
					$error = array('error' => $this->upload->display_errors());
					print_r($error['error']);
					$success = false;
				} else {
					$dataUpdate = array(
						'id' => $slidersId,
						'imageMobile' => $imageMobile,
						'title' => $title,
						'description' => $description,
						'link' => $link,
						'status' => $status
					);
					$update = $this->sliderModel->updateSliderWIthImageMobile($dataUpdate);
					if($update) {
						$success = true;
					} else {
						$success = false;
					}
				}
			} else {
				$dataUpdate = array(
					'id' => $slidersId,
					'title' => $title,
					'description' => $description,
					'link' => $link,
					'status' => $status
				);
				$update = $this->sliderModel->updateSlider($dataUpdate);
				if($update) {
					$success = true;
				} else {
					$success = false;
				}
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateSlider', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateSlider', $data);
		}
		redirect('/backend/sliders/editSlider/'.$slidersId, 'refresh');
	}

	public function getDataDelete() {
		$sliderId = $this->input->post('id');
		$dataDelete = $this->sliderModel->getDataDelete($sliderId);
		echo json_encode($dataDelete);
	}
	
	public function removeStatusUpdateSlider() {
		$this->session->unset_userdata('statusUpdateSlider');
	}

	public function removeSlider() {
		$params = $this->uri->segment(4);
		$sliderId = $this->uri->segment(5);
		$redirectId = $this->uri->segment(6);
		$page = '';
		$url = '';
		if($params == 'new') {
			$page = 'sliders';
			$url = "/backend/sliders/";
		} else {
			$page = 'editSlider';
			$url = "/backend/sliders/".$page."/".$redirectId;
		}
		$success = true;
		$data = [];
		$removeSlider = $this->sliderModel->removeSlider($sliderId);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusSlider', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusSlider', $data);
		}
		redirect($url, "refresh");
	}

	public function removeStatusHapusSlider() {
		$this->session->unset_userdata('statusHapusSlider');
	}
}
