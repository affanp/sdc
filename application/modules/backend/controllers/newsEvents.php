
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class newsEvents extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model("newsEventsModel");
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$sessions = $this->session->all_userdata();
		$dataNewsEvents = $this->newsEventsModel->getDataNewsEvents();
		$dataCategories = $this->newsEventsModel->getDataCategories();
		$this->load->view('newsEvents/v_newsEvents', array(
			'dataNewsEvents' => $dataNewsEvents,
			'dataCategories' => $dataCategories,
			'sessions' => $sessions
		));
	}

	public function saveNewsEvents() {
		$success = true;
		$data = [];
		$category = $this->input->post('category');
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/newsEvents/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'category' => $category,
				'image' => $image,
				'title' => $title,
				'description' => $description,
				'status' => $status
			);
			$saveId = $this->newsEventsModel->saveNewsEvents($dataSave);
			if($saveId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveNewsEvents', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveNewsEvents', $data);
		}
		redirect('/backend/newsEvents', 'refresh');
	}

	public function removeStatusSaveNewsEvents() {
		$this->session->unset_userdata('statusSaveNewsEvents');
	}

	public function editNewsEvents() {
		$id = $this->uri->segment(4);
		$detailNewsEvents = $this->newsEventsModel->getDetailNewsEvents($id);
		$dataCategories = $this->newsEventsModel->getDataCategories();
		$dataNewsEventsEdit = $this->newsEventsModel->getDataNewsEventsEdit($id);
		$sessions = $this->session->all_userdata();
		$this->load->view('newsEvents/v_editNewsEvents', array(
			'detailNewsEvents' => $detailNewsEvents,
			'dataCategories' => $dataCategories,
			'dataNewsEvents' => $dataNewsEventsEdit,
			'sessions' => $sessions
		));
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->newsEventsModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function updateNewsEvents() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$category = $this->input->post('category');
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/newsEvents/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'category' => $category,
					'image' => $image,
					'title' => $title,
					'description' => $description,
					'status' => $status
				);
				$updateId = $this->newsEventsModel->updateNewsEventsWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'category' => $category,
				'title' => $title,
				'description' => $description,
				'status' => $status
			);
			$updateId = $this->newsEventsModel->updateNewsEvents($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateNewsEvents', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateNewsEvents', $data);
		}
		redirect('/backend/newsEvents/editNewsEvents/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateNewsEvents() {
		$this->session->unset_userdata('statusUpdateNewsEvents');
	}

	public function removeNewsEvents() {
		$params = $this->uri->segment(4);
		$idDeleted = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$page = '';
		if($params == 'new') {
			$page = "/backend/newsEvents/";
		} else {
			$page = "/backend/newsEvents/editNewsEvents/".$idBack;
		}
		$success = true;
		$data = [];
		$deletedNewsEvents = $this->newsEventsModel->removeNewsEvents($idDeleted);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusNewsEvents', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusNewsEvents', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusNewsEvents() {
		$this->session->unset_userdata('statusHapusNewsEvents');
	}
}
