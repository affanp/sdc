
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Articles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model("articleModel");
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$sessions = $this->session->all_userdata();
		$dataArticles = $this->articleModel->getDataArticle();
		$this->load->view('articles/v_articles', array(
			'dataArticles' => $dataArticles,
			'sessions' => $sessions
		));
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->articleModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function saveArticle() {
		$success = true;
		$data = [];
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/article/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'image' => $image,
				'title' => $title,
				'description' => $description,
				'status' => $status
			);
			$insertId = $this->articleModel->saveArticle($dataSave);
			if($insertId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveArticle', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveArticle', $data);
		}
		redirect('/backend/articles', 'refresh');
	}

	public function removeStatusSaveArticle() {
		$this->session->unset_userdata('statusSaveArticle');
	}

	public function editArticle() {
		$sessions = $this->session->all_userdata();
		$idArticle = $this->uri->segment(4);
		$detailArticle = $this->articleModel->getDetailArticle($idArticle);
		$dataArticleEdit = $this->articleModel->getDataArticleEdit($idArticle);
		$this->load->view('articles/v_editArticle', array(
			'detailArticle' => $detailArticle,
			'dataArticle' => $dataArticleEdit,
			'sessions' => $sessions
		));
	}

	public function updateArticle() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/article/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'image' => $image,
					'title' => $title,
					'description' => $description,
					'status' => $status
				);
				$updateId = $this->articleModel->updateArticleWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'title' => $title,
				'description' => $description,
				'status' => $status
			);
			$updateId = $this->articleModel->updateArticle($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateArticle', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateArticle', $data);
		}
		redirect('/backend/articles/editArticle/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateArticle() {
		$this->session->unset_userdata('statusUpdateArticle');
	}

	public function removeArticle() {
		$params = $this->uri->segment(4);
		$idArticle = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$page = '';
		if($params == 'new') {
			$page = "/backend/articles/";
		} else {
			$page = "/backend/articles/editArticle/".$idBack;
		}
		$success = true;
		$data = [];
		$deleteArticle = $this->articleModel->deleteArticle($idArticle);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusArticle', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusArticle', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusArticle() {
		$this->session->unset_userdata('statusHapusArticle');
	}

}
