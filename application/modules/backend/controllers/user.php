
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('userModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$sessions = $this->session->all_userdata();
		if($sessions['userType'] != 1) {
			redirect('/backend/dashboard', 'refresh');
		} else {
			$dataUser = $this->userModel->getDataUser();
		$dataUserType = $this->userModel->getDataUserType();
		$this->load->view('user/v_user', array(
			'dataUser' => $dataUser,
			'dataUserType' => $dataUserType
		));
		}
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->userModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function saveUser() {
		$success = true;
		$data = [];
		$image = $_FILES['image']['name'];
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password = password_hash($password, PASSWORD_DEFAULT);
		$status = $this->input->post('status');
		$type = $this->input->post('type');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/user/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'image' => $image,
				'name' => $name,
				'email' => $email,
				'password' => $password,
				'status' => $status,
				'type' => $type
			);
			$saveId = $this->userModel->saveUser($dataSave);
			if($saveId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveUser', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveUser', $data);
		}
		redirect('/backend/user', 'refresh');
	}

	public function removeStatusSaveUser() {
		$this->session->unset_userdata('statusSaveUser');
	}

	public function editUser() {
		$id = $this->uri->segment(4);
		$detailUser = $this->userModel->getDetailUser($id);
		$dataUser = $this->userModel->getDataUserEdit($id);
		$dataUserType = $this->userModel->getDataUserType();
		$this->load->view('user/v_editUser', array(
			'detailUser' => $detailUser,
			'dataUser' => $dataUser,
			'dataUserType' => $dataUserType
		));
	}

	public function updateUser() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$image = $_FILES['image']['name'];
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$status = $this->input->post('status');
		$type = $this->input->post('type');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/user/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'image' => $image,
					'name' => $name,
					'email' => $email,
					'status' => $status,
					'type' => $type
				);
				$updateId = $this->userModel->updateUserWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'name' => $name,
				'email' => $email,
				'status' => $status,
				'type' => $type
			);
			$updateId = $this->userModel->updateUser($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateUser', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateUser', $data);
		}
		redirect('/backend/user/editUser/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateUser() {
		$this->session->unset_userdata('statusUpdateUser');
	}

	public function removeUser() {
		$params = $this->uri->segment(4);
		$idDelete = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$delete = $this->userModel->deleteUser($idDelete);
		$page = '';
		if($params == 'new') {
			$page = "/backend/user/";
		} else {
			$page = "/backend/user/editUser/".$idBack;
		}
		$success = true;
		$data = [];
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusUser', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusUser', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusUser() {
		$this->session->unset_userdata('statusHapusUser');
	}

}
