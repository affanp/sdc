
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Videos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('videosModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$dataVideos = $this->videosModel->getDataVideos();
		$sessions = $this->session->all_userdata();
		$this->load->view('videos/v_videos', array(
			'dataVideos' => $dataVideos,
			'sessions' => $sessions
		));
	}

	public function saveVideos() {
		$success = true;
		$data = [];
		$title = $this->input->post('title');
		$link = $this->input->post('link');
		$link = str_replace("watch", "embed", $link);
		$dataSave = array(
			'title' => $title,
			'link' => $link
		);
		$saveVideos = $this->videosModel->saveVideos($dataSave);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveArticle', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveArticle', $data);
		}
		redirect('/backend/videos', 'refresh');
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->videosModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function editvideos() {
		$id = $this->uri->segment(4);
		$detailVideos = $this->videosModel->getDetailVideos($id);
		$dataVideos = $this->videosModel->getDataVideoEdit($id);
		$sessions = $this->session->all_userdata();
		$this->load->view('videos/v_editVideos', array(
			'detailVideos' => $detailVideos,
			'dataVideos' => $dataVideos,
			'sessions' => $sessions
		));
	}

	public function updateVideos() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$link = $this->input->post('link');
		$dataUpdate = array(
			'id' => $id,
			'title' => $title,
			'link' => $link
		);
		$updateId = $this->videosModel->updateVideos($dataUpdate);
		if($updateId) {
			$success = true;
		} else {
			$success = false;
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveArticle', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveArticle', $data);
		}
		redirect('/backend/videos/editVideos/'.$id, 'refresh');
	}

	public function removeVideos() {
		$params = $this->uri->segment(4);
		$idVideos = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$page = '';
		if($params == 'new') {
			$page = "/backend/videos/";
		} else {
			$page = "/backend/videos/editvideos/".$idBack;
		}
		$success = true;
		$data = [];
		$deleteArticle = $this->videosModel->deleteVideos($idVideos);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusVideos', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusVideos', $data);
		}
		redirect($page, "refresh");
	}
}
