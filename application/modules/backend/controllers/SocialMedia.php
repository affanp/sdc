
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class SocialMedia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('socialMediaModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$sessions = $this->session->all_userdata();
		$dataSocialMedia = $this->socialMediaModel->getDataSocialMedia();
		$this->load->view('socialMedia/v_socialMedia', array(
			'dataSocialMedia' => $dataSocialMedia,
			'sessions' => $sessions
		));
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->socialMediaModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function saveSocialMedia() {
		$success = true;
		$data = [];
		$name = $this->input->post('name');
		$image = $_FILES['image']['name'];
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/socialMedia/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'name' => $name,
				'image' => $image,
				'link' => $link,
				'status' => $status
			);
			$saveId = $this->socialMediaModel->saveSocialMedia($dataSave);
			if($saveId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveSocialMedia', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveSocialMedia', $data);
		}
		redirect('/backend/socialMedia', 'refresh');
	}

	public function removeStatusSaveSocialMedia() {
		$this->session->unset_userdata('statusSaveSocialMedia');
	}

	public function editSocialMedia() {
		$id = $this->uri->segment(4);
		$detailSocialMedia = $this->socialMediaModel->getDetailSocialMedia($id);
		$dataSocialMedia = $this->socialMediaModel->getDataSocialMediaEdit($id);
		$sessions = $this->session->all_userdata();
 		$this->load->view('socialMedia/v_editSocialMedia', array(
			'detailSocialMedia' => $detailSocialMedia,
			'dataSocialMedia' => $dataSocialMedia,
			'sessions' => $sessions
		));
	}

	public function updateSocialMedia() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$image = $_FILES['image']['name'];
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/socialMedia/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'name' => $name,
					'image' => $image,
					'link' => $link,
					'status' => $status
				);
				$updateId = $this->socialMediaModel->updateSocialMediaWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'name' => $name,
				'link' => $link,
				'status' => $status
			);
			$updateId = $this->socialMediaModel->updateSocialMedia($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateSocialMedia', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateSocialMedia', $data);
		}
		redirect('/backend/socialMedia/editSocialMedia/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateSocialMedia() {
		$this->session->unset_userdata('statusUpdateSocialMedia');
	}

	public function removeSocialMedia() {
		$params = $this->uri->segment(4);
		$idDelete = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$deleteSocialMedia = $this->socialMediaModel->deleteSocialMedia($idDelete);
		$page = '';
		if($params == 'new') {
			$page = "/backend/socialMedia/";
		} else {
			$page = "/backend/socialMedia/editSocialMedia/".$idBack;
		}
		$success = true;
		$data = [];
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusSocialMedia', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusSocialMedia', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusSocialMedia() {
		$this->session->unset_userdata('statusHapusSocialMedia');
	}

}
