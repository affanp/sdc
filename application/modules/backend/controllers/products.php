
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Products extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('productsModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$sessions = $this->session->all_userdata();
		$dataProducts = $this->productsModel->getDataProducts();
		$this->load->view('products/v_products', array(
			'dataProducts' => $dataProducts,
			'sessions' => $sessions
		));
	}

	public function saveProduct() {
		$success = true;
		$data = [];
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$config['upload_path']          = './assets/backend/uploads/products/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'image' => $image,
				'title' => $title,
				'description' => $description
			);
			$saveId = $this->productsModel->saveProduct($dataSave);
			if($saveId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveProduct', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveProduct', $data);
		}
		redirect('/backend/products', 'refresh');
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->productsModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function removeStatusSaveProduct() {
		$this->session->unset_userdata('statusSaveProduct');
	}

	public function editProduct() {
		$id = $this->uri->segment(4);
		$detailProduct = $this->productsModel->getDetailProduct($id);
		$dataProductEdit = $this->productsModel->getDataProductEdit($id);
		$sessions = $this->session->all_userdata();
		$this->load->view('products/v_editProduct', array(
			'detailProduct' => $detailProduct,
			'dataProductEdit' => $dataProductEdit,
			'sessions' => $sessions
		));
	}

	public function updateProduct() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/products/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'image' => $image,
					'title' => $title,
					'description' => $description
				 );
				$updateId = $this->productsModel->updateProductWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'title' => $title,
				'description' => $description
 			);
			 $updateId = $this->productsModel->updateProduct($dataUpdate);
			 if($updateId) {
				 $success = true;
			 } else {
				 $success = false;
			 }
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateProduct', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateProduct', $data);
		}
		redirect('/backend/products/editProduct/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateProduct() {
		$this->session->unset_userdata('statusUpdateProduct');
	}

	public function removeProduct() {
		$params = $this->uri->segment(4);
		$idDelete = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$page = '';
		if($params == 'new') {
			$page = "/backend/products/";
		} else {
			$page = "/backend/products/editProduct/".$idBack;
		}
		$success = true;
		$data = [];
		$delete = $this->productsModel->deleteProduct($idDelete);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusProduct', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusProduct', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusProduct() {
		$this->session->unset_userdata('statusHapusProduct');
	}

}
