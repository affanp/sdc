
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Backend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('backendModel');
	}
 
	public function index() {
		return;
	}

	public function login() {
		$this->session->sess_destroy();
		$this->load->view('login/v_login', array(
			
		));
	}

	public function prosesLogin() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$dataPassword = $this->backendModel->getPassword($email);
		$hashPassword = $dataPassword['password'];
		if($dataPassword) {
			// it's mean that the email is exist
			if(password_verify($password, $hashPassword)) {
				$dataUsers = $this->backendModel->getUsers($email);
				$this->session->set_userdata('userEmail', $email);
				$this->session->set_userdata('id', $dataUsers['id']);
				$this->session->set_userdata('image', $dataUsers['image']);
				$this->session->set_userdata('name', $dataUsers['name']);
				$this->session->set_userdata('status', $dataUsers['status']);
				$this->session->set_userdata('userType', $dataUsers['user_type']);
				redirect('/backend/dashboard', 'refresh');
			} else {
				echo "Email atau Password yang anada masukan salah";
			}
		}
	}

	

	public function marketplace() {
		$this->load->view('marketplace/v_marketplace', array(
			
		));
	}
	public function videos() {
		$this->load->view('videos/v_videos', array(
			
		));
	}
	public function articles() {
		$this->load->view('articles/v_articles', array(
			
		));
	}
	public function newsEvents() {
		$this->load->view('newsEvents/v_newsEvents', array(
			
		));
	}
	public function products() {
		$this->load->view('products/v_products', array(
			
		));
	}
	public function composition() {
		$this->load->view('composition/v_composition', array(
			
		));
	}
	public function socialMedia() {
		$this->load->view('socialMedia/v_socialMedia', array(
			
		));
	}

	public function user() {
		$this->load->view('settings/user/v_user', array(
			
		));
	}

	public function profile() {
		$this->load->view('settings/profile/v_profile', array(
			
		));
	}
}
