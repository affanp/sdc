
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Composition extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('compositionModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$dataComposition = $this->compositionModel->getDataComposition();
		$this->load->view('composition/v_composition', array(
			'dataComposition' => $dataComposition
		));
	}

	public function getDataDelete() {
		$id = $this->input->post('id');
		$dataDelete = $this->compositionModel->getDataDelete($id);
		echo json_encode($dataDelete);
	}

	public function saveComposition() {
		$success = true;
		$data = [];
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$dataSave = array(
			'image' => $image,
			'title' => $title,
			'description' => $description
		);
		$config['upload_path']          = './assets/backend/uploads/composition/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $image;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$image = str_replace(" ", "_", $image);
			$dataSave = array(
				'image' => $image,
				'title' => $title,
				'description' => $description
			);
			$updateId = $this->compositionModel->saveComposition($dataSave);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveComposition', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveComposition', $data);
		}
		redirect('/backend/composition', 'refresh');
	}

	public function removeStatusSaveComposition() {
		$this->session->unset_userdata('statusSaveComposition');
	}

	public function editComposition() {
		$id = $this->uri->segment(4);
		$detailComposition = $this->compositionModel->getDetailComposition($id);
		$dataComposition = $this->compositionModel->getDataCompositionEdit($id);
		$this->load->view('composition/v_editComposition', array(
			'detailComposition' => $detailComposition,
			'dataComposition' => $dataComposition
		));
	}

	public function updateComposition() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$image = $_FILES['image']['name'];
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/composition/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'image' => $image,
					'title' => $title,
					'description' => $description
				);
				$updateId = $this->compositionModel->updateCompositionWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'title' => $title,
				'description' => $description
			);
			$updateId = $this->compositionModel->updateComposition($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusUpdateComposition', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusUpdateComposition', $data);
		}
		redirect('/backend/composition/editComposition/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateComposition() {
		$this->session->unset_userdata('statusUpdateComposition');
	}

	public function removeComposition() {
		$params = $this->uri->segment(4);
		$idDelete = $this->uri->segment(5);
		$idBack = $this->uri->segment(6);
		$deleteComposition = $this->compositionModel->deleteComposition($idDelete);
		$page = '';
		if($params == 'new') {
			$page = "/backend/composition/";
		} else {
			$page = "/backend/composition/editComposition/".$idBack;
		}
		$success = true;
		$data = [];
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusComposition', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusComposition', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusComposition() {
		$this->session->unset_userdata('statusHapusComposition');
	}
	
}
