
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Marketplace extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('marketplaceModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$dataMarketplace = $this->marketplaceModel->getDataMarketplace();
		$sessions = $this->session->all_userdata();
		$this->load->view('marketplace/v_marketplace', array(
			'dataMarketplace' => $dataMarketplace,
			'sessions' => $sessions
		));
	}

	public function getDataDelete() {
		$marketplaceId = $this->input->post('id');
		$dataDelete = $this->marketplaceModel->getDataDelete($marketplaceId);
		echo json_encode($dataDelete);
	}

	public function saveMarketplace() {
		$success = true;
		$data = [];
		$imageName = $_FILES['image']['name'];
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		$config['upload_path']          = './assets/backend/uploads/marketplace/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['file_name']			= $imageName;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error['error']);
			$success = false;
		} else {
			$dataSave = array(
				'image' => $imageName,
				'link' => $link,
				'status' => $status
			);
			$saveId = $this->marketplaceModel->saveMarketplace($dataSave);
			if($saveId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Data Success";
			$this->session->set_userdata('statusSaveMarketplace', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Your Data Failed";
			$this->session->set_userdata('statusSaveMarketplace', $data);
		}
		redirect('/backend/marketplace', 'refresh');
	}

	public function removeStatusSaveMarketplace() {
		$this->session->unset_userdata('statusSaveMarketplace');
	}

	public function editMarketPlace() {
		$idMarketplace = $this->uri->segment(4);
		$detailMarketpalce = $this->marketplaceModel->getDetailMarketplace($idMarketplace);
		$dataMarketplace = $this->marketplaceModel->getDataMarketplaceEdit($idMarketplace);
		$sessions = $this->session->all_userdata();
		$this->load->view('marketplace/v_editMarketplace', array(
			'dataMarketplace' => $detailMarketpalce,
			'dataMarketplaceEdit' => $dataMarketplace,
			'sessions' => $sessions
		));
	}

	public function updateMarketplace() {
		$success = true;
		$data = [];
		$id = $this->input->post('id');
		$logo = $_FILES['logo']['name'];
		$link = $this->input->post('link');
		$status = $this->input->post('status');
		if($status) {
			$status = 1;
		} else {
			$status = 2;
		}
		if($logo) {
			$config['upload_path']          = './assets/backend/uploads/marketplace/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $logo;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('logo')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$logo = str_replace(" ", "_", $logo);
				$dataUpdate = array(
					'id' => $id,
					'logo' => $logo,
					'link' => $link,
					'status' => $status
				);
				$updateId = $this->marketplaceModel->updateMarketplaceWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$dataUpdate = array(
				'id' => $id,
				'link' => $link,
				'status' => $status
			);
			$updateId = $this->marketplaceModel->updateMarketplace($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		if($success) {
			$data['success'] = true;
			$data['message'] = "Your Marketplace Successfully Updated";
			$this->session->set_userdata('statusUpdateMarketplace', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "the update was Failed";
			$this->session->set_userdata('statusUpdateMarketplace', $data);
		}
		redirect('/backend/marketplace/editMarketplace/'.$id, 'refresh');
	}
	
	public function removeStatusUpdateMarketplace() {
		$this->session->unset_userdata('statusUpdateMarketplace');
	}

	public function removeMarketplace() {
		$params = $this->uri->segment(4);
		$idBack = $this->uri->segment(6);
		$idMarketplace = $this->uri->segment(5);
		$page = '';
		if($params == 'new') {
			$page = "/backend/marketplace/";
		} else {
			$page = "/backend/marketplace/editMarketplace/".$idBack;
		}
		$success = true;
		$data = [];
		$removeMarketplace = $this->marketplaceModel->removeMarketplace($idMarketplace);
		if($success) {
			$data['success'] = true;
			$data['message'] = "Data Berhasil di Hapus";
			$this->session->set_userdata('statusHapusMarketplace', $data);
		} else {	
			$data['success'] = false;
			$data['message'] = "Data Gagal di Hapus";
			$this->session->set_userdata('statusHapusMarketplace', $data);
		}
		redirect($page, "refresh");
	}

	public function removeStatusHapusMarketplace() {
		$this->session->unset_userdata('statusHapusMarketplace');
	}

}
