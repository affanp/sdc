
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('be_helper');
		$this->load->library(array('session'));
		$this->load->model('profileModel');
		$sessions = $this->session->all_userdata();
		if(!$sessions['userEmail']){
			redirect('/backend/login', 'refresh');
		}
	}
 
	public function index() {
		$email = $this->session->userdata['userEmail'];
		$dataUser = $this->profileModel->getDataUser($email);
		$this->load->view('profile/v_profile', array(
			'dataUser' => $dataUser
		));
	}

	public function updateUser() {
		$success = true;
		$id = $this->input->post('id');
		$image = $_FILES['image']['name'];
		$email = $this->input->post('email');
		$name = $this->input->post('name');
		if($image) {
			$config['upload_path']          = './assets/backend/uploads/user/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['file_name']			= $image;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error['error']);
				$success = false;
			} else {
				$image = str_replace(" ", "_", $image);
				$dataUpdate = array(
					'id' => $id,
					'image' => $image,
					'name' => $name,
					'email' => $email
				);
				$updateId = $this->profileModel->updateUserWithImage($dataUpdate);
				if($updateId) {
					$success = true;
				} else {
					$success = false;
				}
			}
		} else {
			$image = str_replace(" ", "_", $image);
			$dataUpdate = array(
				'id' => $id,
				'name' => $name,
				'email' => $email
			);
			$updateId = $this->profileModel->updateUser($dataUpdate);
			if($updateId) {
				$success = true;
			} else {
				$success = false;
			}
		}
		$this->session->set_userdata('userEmail', $email);

		if($success) {
			redirect('/backend/profile', 'refresh');
		} else {
			echo "gagal update profile";	
		}
	}

	public function updateProfile() {
		$id = $this->input->post('id');
		$dataPassword = $this->profileModel->getDataPassword($id);
		$oldPassword = $this->input->post('oldPassword');
		$newPassword = $this->input->post('newPassword');
		$confirmPassword = $this->input->post('confirmPassword');
		if(!password_verify($oldPassword, $dataPassword['password'])){
			echo "Old Password salah";
		} else {
			if($newPassword != $confirmPassword) {
				echo "password baru tidak bisa di confirm";
			} else {
				// $password = password_hash($password, PASSWORD_DEFAULT);
				$newPassword = password_hash($newPassword, PASSWORD_DEFAULT);
				$dataUpdate = array(
					'id' => $id,
					'newPassword' => $newPassword
				);
				$update = $this->profileModel->updatePassword($dataUpdate);
				redirect('/backend/profile', 'refresh');
			}
		}
	}

}
