<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class VideosModel extends CI_Model {
 
    public function saveVideos($data) {
        $query = $this->db->query("INSERT INTO videos (title, link) VALUES ('".$data['title']."', '".$data['link']."')");
        return $this->db->insert_id();
    }

    public function getDataVideos() {
        $query = $this->db->query("SELECT id, title, link FROM videos ORDER BY id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT title FROM videos WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteVideos($id) {
        $query = $this->db->query("DELETE FROM videos WHERE id = '".$id."'");
        return true;
    }

    public function getDetailVideos($id) {
        $query = $this->db->query("SELECT id, title, link FROM videos WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function getDataVideoEdit($id) {
        $query = $this->db->query("SELECT id, title, link FROM videos WHERE id != '".$id."'");
        return $query->result_array();
    }

    public function updateVideos($data) {
        $query = $this->db->query("UPDATE videos SET title = '".$data['title']."', link = '".$data['link']."' WHERE id = '".$data['id']."'");
        return true;
    }

 }
