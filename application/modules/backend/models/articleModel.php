<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class ArticleModel extends CI_Model {
 
    public function saveArticle($data) {
        $query = $this->db->query("INSERT INTO articles (image, title, description, status) VALUES ('".$data['image']."', '".$data['title']."', '".$data['description']."', '".$data['status']."') ");
        return $this->db->insert_id();
    }

    public function getDataArticle() {
        $query = $this->db->query("SELECT id, image, title, description, status FROM articles ORDER BY id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT title FROM articles WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteArticle($id) {
        $query = $this->db->query("DELETE FROM articles WHERE id = '".$id."'");
        return true;
    }

    public function getDetailArticle($id) {
        $query = $this->db->query("SELECT id, image, title, description, status FROM articles WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function updateArticleWithImage($data) {
        $query = $this->db->query("UPDATE articles SET image = '".$data['image']."', title = '".$data['title']."', description = '".$data['description']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateArticle($data) {
        $query = $this->db->query("UPDATE articles SET title = '".$data['title']."', description = '".$data['description']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataArticleEdit($id) {
        $query = $this->db->query("SELECT id, image, title, description, status FROM articles WHERE id != '".$id."'");
        return $query->result_array();
    }
 }
