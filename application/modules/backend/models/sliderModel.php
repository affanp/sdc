<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class SliderModel extends CI_Model {
 
    public function saveSlider($data) {
        $query = $this->db->query("INSERT INTO sliders (image, image_mobile, title, description, link, status) VALUES ('".$data['image']."', '".$data['imageMobile']."', '".$data['title']."', '".$data['description']."', '".$data['link']."', '".$data['status']."') ");
        return $this->db->insert_id();
    }

    public function getDataSlider() {
        $query = $this->db->query("SELECT sliders.id as slidersId, sliders.image as slidersImage, sliders.image_mobile as slidersImageMobile, sliders.title as slidersTitle, sliders.description as slidersDescription, sliders.link as slidersLink, sliders.status as slidersStatus, status.name as statusName FROM sliders JOIN status ON status.id = sliders.status ORDER BY sliders.id DESC");
        return $query->result_array();
    }

    public function getDetailSlider($slidersId) {
        $query = $this->db->query("SELECT sliders.id as slidersId, sliders.image as slidersImage, sliders.image_mobile as slidersImageMobile, sliders.title as slidersTitle, sliders.description as slidersDescription, sliders.link as slidersLink, sliders.status as slidersStatus FROM sliders JOIN status ON sliders.status = status.id WHERE sliders.id = $slidersId");
        return $query->row_array();
    }

    public function updateSliderWIthImage($data) {
        $query = $this->db->query("UPDATE sliders SET image = '".$data['image']."', title = '".$data['title']."', description = '".$data['description']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateSliderWIthImages($data) {
        $query = $this->db->query("UPDATE sliders SET image = '".$data['image']."', image_mobile = '".$data['imageMobile']."', title = '".$data['title']."', description = '".$data['description']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateSlider($data) {
        $query = $this->db->query("UPDATE sliders SET title = '".$data['title']."', description = '".$data['description']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataSliderEdit($slidersId) {
        $query = $this->db->query("SELECT sliders.id as slidersId, sliders.image as slidersImage, sliders.title as slidersTitle, sliders.description as slidersDescription, sliders.link as slidersLink, sliders.status as slidersStatus, status.name as statusName FROM sliders JOIN status ON status.id = sliders.status WHERE sliders.id != $slidersId ORDER BY sliders.id DESC");
        return $query->result_array();
    }

    public function updateSliderWIthImageMobile($data) {
        $query = $this->db->query("UPDATE sliders SET image_mobile = '".$data['imageMobile']."', title = '".$data['title']."', description = '".$data['description']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT title FROM sliders WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function removeSlider($sliderId) {
        $query = $this->db->query("DELETE FROM sliders WHERE id = '".$sliderId."'");
        return true;
    }
}
