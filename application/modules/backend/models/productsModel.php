<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class ProductsModel extends CI_Model {
    public function saveProduct($data) {
        $query = $this->db->query("INSERT INTO products (image, title, description) VALUES ('".$data['image']."', '".$data['title']."', '".$data['description']."')");
        return $this->db->insert_id();
    }

    public function getDataProducts() {
        $query = $this->db->query("SELECT id, image, title, description FROM products ORDER BY id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT title FROM products WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteProduct($id) {
        $query = $this->db->query("DELETE FROM products WHERE id = '".$id."'");
        return true;
    }

    public function getDetailProduct($id) {
        $query = $this->db->query("SELECT id, image, title, description FROM products WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function getDataProductEdit($id) {
        $query = $this->db->query("SELECT id, image, title, description FROM products WHERE id != '".$id."'");
        return $query->result_array();
    }

    public function updateProductWithImage($data) { 
        $query = $this->db->query("UPDATE products SET image = '".$data['image']."', title = '".$data['title']."', description = '".$data['description']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateProduct($data) {
        $query = $this->db->query("UPDATE products SET title = '".$data['title']."', description = '".$data['description']."' WHERE id = '".$data['id']."'");
        return true;
    }
 }
