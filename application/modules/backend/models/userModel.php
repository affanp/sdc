<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class UserModel extends CI_Model {
 
    public function saveUser($data) {
        $query = $this->db->query("INSERT INTO user (image, name, email, password, status, user_type) VALUES ('".$data['image']."', '".$data['name']."', '".$data['email']."', '".$data['password']."', '".$data['status']."', '".$data['type']."')");
        return $this->db->insert_id();
    }

    public function getDataUser() {
        $query = $this->db->query("SELECT user.id as userId, user.image as userImage, user.name as userName, user.email as userEmail, user.password as userPassword, user.status as userStatus, user.user_type as userType, user_type.name as typeName FROM user JOIN user_type ON user.user_type = user_type.id ORDER BY user.id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT name FROM user WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteUser($id) {
        $query = $this->db->query("DELETE FROM user WHERE id = '".$id."'");
        return true;
    }

    public function getDetailUser($id) {
        $query = $this->db->query("SELECT id, image, name, email, password, status, user_type FROM user WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function updateUserWithImage($data) {
        $query = $this->db->query("UPDATE USER SET image = '".$data['image']."', name = '".$data['name']."', email = '".$data['email']."', status = '".$data['status']."', user_type = '".$data['type']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateUser($data) {
        $query = $this->db->query("UPDATE USER SET name = '".$data['name']."', email = '".$data['email']."', status = '".$data['status']."', user_type = '".$data['type']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataUserEdit($id) {
        $query = $this->db->query("SELECT user.id as userId, user.image as userImage, user.name as userName, user.email as userEmail, user.status as userStatus, user.user_type as userType, user_type.name as typeName FROM user JOIN user_type ON user.user_type = user_type.id WHERE user.id != '".$id."'");
        return $query->result_array();
    }

    public function getDataUserType() {
        $query = $this->db->query("SELECT id, name, created_at FROM user_type ORDER BY id ASC");
        return $query->result_array();
    }
 }
