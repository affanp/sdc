<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class MarketplaceModel extends CI_Model {
 
    public function saveMarketplace($data) {
        $query = $this->db->query("INSERT INTO marketplace (logo, link, status) VALUES ('".$data['image']."', '".$data['link']."', '".$data['status']."') ");
        return $this->db->insert_id();
    }

    public function getDataMarketplace() {
        $query = $this->db->query("SELECT marketplace.id as idMarketplace, marketplace.logo as logoMarketplace, marketplace.link as linkMarketplace, marketplace.status as statusMarketplace, status.name as statusName FROM marketplace JOIN status ON marketplace.status = status.id ORDER BY marketplace.id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT id FROM marketplace WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function removeMarketplace($id) {
        $query = $this->db->query("DELETE FROM marketplace WHERE id = '".$id."'");
        return true;
    }

    public function getDetailMarketplace($id) {
        $query = $this->db->query("SELECT id, logo, link, status FROM marketplace WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function updateMarketplaceWithImage($data) {
        $query = $this->db->query("UPDATE marketplace SET logo = '".$data['logo']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");;
        return true;
    }

    public function updateMarketplace($data) {
        $query = $this->db->query("UPDATE marketplace SET link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");;
        return true;
    }

    public function getDataMarketplaceEdit($id) {
        $query = $this->db->query("SELECT id, logo, link, status FROM marketplace WHERE id != '".$id."'");
        return $query->result_array();
    }
 }
