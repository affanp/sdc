<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class SocialMediaModel extends CI_Model {
 
    public function saveSocialMedia($data) {
        $query = $this->db->query("INSERT INTO social_media (name, image, link, status) VALUES ('".$data['name']."', '".$data['image']."', '".$data['link']."', '".$data['status']."') ");
        return $this->db->insert_id();
    }

    public function getDataSocialMedia() {
        $query = $this->db->query("SELECT id, name, image, link, status FROM social_media ORDER BY id");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT name FROM social_media WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteSocialMedia($id) {
        $query = $this->db->query("DELETE FROM social_media WHERE id = '".$id."'");
        return true;
    }

    public function getDetailSocialMedia($id) {
        $query = $this->db->query("SELECT id, name, image, link, status FROM social_media WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function updateSocialMediaWithImage($data) {
        $query = $this->db->query("UPDATE social_media SET name = '".$data['name']."', image = '".$data['image']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }
    
    public function updateSocialMedia($data) {
        $query = $this->db->query("UPDATE social_media SET name = '".$data['name']."', link = '".$data['link']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataSocialMediaEdit($id) {
        $query = $this->db->query("SELECT id, name, image, link, status FROM social_media WHERE id != '".$id."'");
        return $query->result_array();
    }
 }
