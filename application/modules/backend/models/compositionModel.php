<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class CompositionModel extends CI_Model {
 
    public function saveComposition($data) {
        $query = $this->db->query("INSERT INTO composition (image, title, description) VALUES ('".$data['image']."', '".$data['title']."', '".$data['description']."') ");
        return $this->db->insert_id();
    }

    public function getDataComposition() {
        $query = $this->db->query("SELECT id, image, title, description FROM composition ORDER BY id DESC");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT title FROM composition WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function deleteComposition($id) {
        $query = $this->db->query("DELETE FROM composition WHERE id = '".$id."'");
        return true;
    }

    public function getDetailComposition($id) {
        $query = $this->db->query("SELECT id, image, title, description FROM composition WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function updateCompositionWithImage($data) {
        $query = $this->db->query("UPDATE composition SET image = '".$data['image']."', title = '".$data['title']."', description = '".$data['description']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateComposition($data) {
        $query = $this->db->query("UPDATE composition SET title = '".$data['title']."', description = '".$data['description']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataCompositionEdit($id) {
        $query = $this->db->query("SELECT id, image, title, description FROM composition WHERE id != '".$id."'");
        return $query->result_array();
    }
 }
