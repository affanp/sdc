<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class NewsEventsModel extends CI_Model {
    public function saveNewsEvents($data) {
        $query = $this->db->query("INSERT INTO news_events (image, title, description, category, status) VALUES ('".$data['image']."', '".$data['title']."', '".$data['description']."', '".$data['category']."', '".$data['status']."')");
        return $this->db->insert_id();
    }

    public function getDataNewsEvents() {
        $query = $this->db->query("SELECT news_events.id as idNewsEvents, news_events.status as statusNewsEvents, news_events.image as imageNewsEvents, news_events.title as titleNewsEvents, news_events.description as descriptionNewsEvents, categories.name as nameCategory FROM news_events JOIN categories ON news_events.category = categories.id ORDER BY news_events.id");
        return $query->result_array();
    }

    public function getDataDelete($id) {
        $query = $this->db->query("SELECT news_events.title as title FROM news_events WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function removeNewsEvents($id) {
        $query = $this->db->query("DELETE FROM news_events WHERE id = '".$id."'");
        return true;
    }

    public function getDetailNewsEvents($id) {
        $query = $this->db->query("SELECT id, image, title, description, category, status FROM news_events WHERE id = '".$id."'");
        return $query->row_array();
    }

    public function getDataCategories() {
        $query = $this->db->query("SELECT id, name FROM categories ORDER BY id DESC");
        return $query->result_array();
    }

    public function updateNewsEventsWithImage($data) {
        $query = $this->db->query("UPDATE news_events SET image = '".$data['image']."', title = '".$data['title']."', description = '".$data['description']."', category = '".$data['category']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function updateNewsEvents($data) {
        $query = $this->db->query("UPDATE news_events SET title = '".$data['title']."', description = '".$data['description']."', category = '".$data['category']."', status = '".$data['status']."' WHERE id = '".$data['id']."'");
        return true;
    }

    public function getDataNewsEventsEdit($id) {
        $query = $this->db->query("SELECT news_events.id as idNewsEvents, news_events.status as statusNewsEvents, news_events.image as imageNewsEvents, news_events.title as titleNewsEvents, news_events.description as descriptionNewsEvents, categories.name as nameCategory FROM news_events JOIN categories ON news_events.category = categories.id WHERE news_events.id != '".$id."'");
        return $query->result_array();
    }
 }
