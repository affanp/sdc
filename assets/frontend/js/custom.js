/* Global jQuery */

/* Contents
// --------------------------------------------- -->
     1. wow animation
     2. Menu Mobile
     5. Owl Slider
     6. Light Box
     7. Fixed Header
*/

(function ($) {
    "use strict";

    /* ------------------  2. Menu Mobile ------------------ */
    var $menu_show = $('.mobile-toggle'),
        $menu = $('header #menu-main'),
        $onepage_menu = $('header a.onepage-mobile-toggle'),
        $onepage_sidebare = $('.fizo-one-page #onpage-sidebar-mobile'),
        $menu_yoga = $('header .yoga-menu'),
        $list = $("ul.nav-menu li a"),
        $list_firo = $("ul.firo-nav-menu li a"),
        $menu_list_firo = $('ul.firo-nav-menu li.has-dropdown'),
        $menu_list = $('header li.has-dropdown'),
        $menu_ul = $('ul.sub-menu'),
        $cart_model = $('.cart-model'),
        $cart_link = $('#cart-link'),
        $search_bar = $('#search_bar'),
        $search_close = $('.close-search'),
        $search_bot = $('#search-header'),
        $fixed_header = $('#fixed-header');

    $menu_show.on("click", function (e) {
        $('#sidebar-menu').slideToggle();
    });
    $('#mobile-toggle-close').on("click", function (e) {
        $('#sidebar-menu').slideToggle();
    });
    $onepage_menu.on("click", function (e) {
        $onepage_sidebare.slideToggle();
    });

    $(".fizo-one-page #onpage-sidebar-mobile ul.fizo-sidebar-menu li a").mouseup(function () {
        $onepage_sidebare.slideToggle();
    });

    $list.on("click", function (event) {
        var submenu = this.parentNode.getElementsByTagName("ul").item(0);
        if (submenu != null) {
            event.preventDefault();
            $(submenu).slideToggle();
        }
    });

    $list_firo.on("click", function (e) {
        var submenu = this.parentNode.getElementsByTagName("ul").item(0);
        if (submenu != null) {
            event.preventDefault();
            $(submenu).slideToggle();
        }
    });

    /*==============================
    Loading
    ==============================*/

    $(window).on('load', function () {
        $('body').imagesLoaded(function () {
            $('.nile-preloader').fadeOut();
        });
    });

    /*==============================
    Animation
    ==============================*/
    $('.animate').scrolla({
        once: true, // only once animation play on scroll
        mobile: false, // disable animation on mobiles 
    });

    /* ------------------  5.Owl Slider ------------------ */
    var home_slider = $("#home-slider");

    home_slider.owlCarousel({
        slideSpeed: 1000,
        responsiveClass:true,
        autoPlay: true,
        loop: true,
        nav: false,
        dots: true,
        margin:10,
        //autoWidth: true,
        //center: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    /* ------------------  6. Light Box ------------------ */
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    /* ------------------  7. Fixed Header ------------------ */
    $(window).on("scroll", function () {
        if ($(window).scrollTop() >= 50) {
            $fixed_header.addClass('fixed-header');
        } else {
            $fixed_header.removeClass('fixed-header');
        }
    });

    $("form").submit(function (event) {
        event.preventDefault();
        return false;
    });

    jQuery("a.yt-preview").YouTubePopUp();

    /* ------------------  Scroll ------------------ */
    $('a[href^="#"]').bind('click.smoothscroll', function (e) {
        e.preventDefault();
        var target = this,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 40
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

    /*==============================================================
    wow animation - on scroll
    ==============================================================*/
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false,
        live: true
    });
    $(window).imagesLoaded(function () {
        wow.init();
    });

    /*==============================================================
    // Animate the scroll to top
    ==============================================================*/
    $(window).on('scroll', function (e) {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });


    $('.go-top').on("click", function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 300);
    })
}(jQuery));
